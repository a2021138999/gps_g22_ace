# pip install requests tzlocal
import json
import requests
from tzlocal import get_localzone
from datetime import datetime, timedelta

timezone = get_localzone()

def getStartCalendarTimestamp():
    today = datetime.now(timezone)
    days_until_monday = today.weekday()
    start_day = today - timedelta(days=days_until_monday)
    return int(start_day.replace(hour=0, minute=0, second=0, microsecond=0).timestamp())

def getEndCalendarTimestamp():
    today = datetime.now(timezone)
    days_until_next_monday = (7 - today.weekday()) % 7
    end_day = today + timedelta(days=days_until_next_monday)
    return int(end_day.replace(hour=0, minute=0, second=0, microsecond=0).timestamp())

timestamp_today = int(datetime.now(timezone).timestamp())
timestamp_start = getStartCalendarTimestamp()
timestamp_end = getEndCalendarTimestamp()

class_title = "PWEB"
class_id = 52009612185368153    # pweb 52009612185368153 gps 52008855454168326
session_id = input("Session ID>> ").replace(" ", "")

r = requests.get("https://inforestudante.ipc.pt/nonio/inscturmas/inscrever.do?args={}".format(class_id), headers={"Cookie": "JSESSIONID={}".format(session_id)})

if r.status_code != 200:
    print("Error: {}".format(r.status_code))
    exit()

r = requests.get("https://inforestudante.ipc.pt/nonio/inscturmas/inscrever.do?method=actualizaDados&notReset=true&_={}&start={}&end={}".format(timestamp_today, timestamp_start, timestamp_end), headers={"Cookie": "JSESSIONID={}".format(session_id)})

if r.status_code != 200:
    print("Error: {}".format(r.status_code))
    exit()

data = json.loads(r.text)
for c in data:
    if class_title in c.get("title"):
        print(c.get("title"))

'''
print(r.text)
{
    "id": "78756",
    "feId": 146731,
    "idTurma": 0,
    "title": "GPSP2(ISEC - L.2.2)",
    "allDay": false,
    "start": "2023-10-26 14:30",
    "end": "2023-10-26 17:30",
    "className": "classeHorario2",
    "editable": false,
    "description": "Gestão de Projeto de Software (60024278)",
    "text": "\u003cb\u003eTurma:\u003c/b\u003e P2\u003cbr/\u003e\u003cb\u003eHora:\u003c/b\u003e 14h30 (3h)\u003cbr/\u003e\u003cb\u003eEdifício/Sala(s):\u003c/b\u003e ISEC (L.2.2)\u003cbr/\u003e\u003cb\u003eDocente(s):\u003c/b\u003e João Carlos Costa Faria da Cunha",
    "inscritosEdicao": 0,
    "inscritosTurma": 0,
    "listaDocentesAula": [],
    "cursosMenoresId": [],
    "cursosMenoresSigla": [],
    "listaEdicoes": [],
    "listaPessoas": [],
    "edificios": [],
    "espacosEdificios": [],
    "siglasEdicoes": [],
    "siglasDocentes": [],
    "nomeEdicoesCodigo": [],
    "entregue": false,
    "reservaId": 0,
    "itemsHistoricoOutrasReservas": [],
    "duracaoSlot": 0,
    "numAluno": 0,
    "folhaPresencasAceite": false,
    "matrId": 0
}
'''