import gzip
import requests
from flask import request, Response

def filter_headers(headers, original_scheme, original_domain, new_scheme, new_domain):
    filtered_headers = dict()
    
    for key, value in headers.items():
        if original_domain in value:
            value = value.replace(original_scheme, new_scheme)
            value = value.replace(original_domain, new_domain)
        
        filtered_headers[key] = value
    
    return filtered_headers

original_scheme = "https"
original_domain = "inforestudante.ipc.pt"

def convert_to_external_headers(headers):
    client_domain = request.host_url.replace(request.scheme + "://", "")[:-1]

    return filter_headers(headers, request.scheme, client_domain, original_scheme, original_domain)

def convert_to_client_headers(tampered_headers):
    client_domain = request.host_url.replace(request.scheme + "://", "")[:-1]

    return filter_headers(tampered_headers, original_scheme, original_domain, request.scheme, client_domain)

def get_full_url(path=None, query_string=None, scheme=None, domain=None):
    scheme = original_scheme if scheme is None else scheme
    domain = original_domain if domain is None else domain

    if path is None:
        return "{}://{}/".format(scheme, domain)
    
    url = "{}://{}/{}".format(scheme, domain, path[1:])

    return url if query_string is None else "{}?{}".format(url, query_string)

def get_external_url(path=None, query_string=None):
    return get_full_url(path, query_string)

def get_client_url(path=None, query_string=None):
    client_domain = request.host_url.replace(request.scheme + "://", "")[:-1]

    return get_full_url(path, query_string, request.scheme, client_domain)

def get_original_request():
    url = get_external_url(request.path, request.query_string.decode())

    return get_tampered_request(request.method, url, convert_to_external_headers(request.headers), request.cookies, request.values)

def get_tampered_request(method, url, headers, cookies, values):
    if method == 'GET':
        response = requests.get(url, headers=headers, cookies=cookies, allow_redirects=False)
    else:
        response = requests.post(url, headers=headers, data=values, cookies=cookies, allow_redirects=False)
    
    return get_tampered_response(response)

def get_tampered_response(original_request):
    headers = convert_to_client_headers(original_request.headers)

    if original_request.is_redirect:
        return Response(status=original_request.status_code, headers=headers)
    
    response_content = original_request.content

    if original_request.headers.get('Transfer-Encoding', '') == 'chunked':
        response_content = b''
        
        for chunk in original_request.iter_content(chunk_size=1024):
            response_content += chunk
        
        del headers['Transfer-Encoding']

    if 'Content-Encoding' in headers and 'gzip' in headers['Content-Encoding']:
        try:
            response_content = gzip.decompress(response_content)
        except gzip.BadGzipFile:
            pass
        
        del headers['Content-Encoding']

    domain = request.host_url.replace(request.scheme + "://", "")[:-1]

    response_content = response_content.replace(get_external_url()[:-1].encode(), get_client_url()[:-1].encode())
    response_content = response_content.replace(original_domain.encode(), domain.encode())

    headers['Content-Length'] = str(len(response_content))

    ret = Response(response_content, status=original_request.status_code)

    for key, value in headers.items():
        ret.headers[key] = value

    for cookie in original_request.cookies:
        ret.set_cookie(cookie.name, cookie.value)

    return ret