import data
import utils
from bs4 import BeautifulSoup
from flask import request, Response

def tamper_enroll_info(soup):
    if soup.find('form', {'id': 'inscreverFormBean'}) is None:
        return soup

    subject_arg = request.query_string.decode('utf-8').split('=')[1]
    subject_id = soup.find('td', {'class': 'subtitle'}).text.strip().split(' - ')[1]

    data.add_subject_id(subject_arg, subject_id)

    tables = soup.find('form', {'id': 'inscreverFormBean'}).findAll('table', {'class': 'displaytable'})

    # Populate subject_classes and subject_enrolls
    for table in tables:
        for row in table.find_all('tr')[1:]:
            enroll_input = row.find_all('td')[5].find('input')
            if enroll_input is None:
                schedule_input = row.find_all('td')[4].find('input')
                if schedule_input is None:
                    continue

                is_enrolled = False
                class_id = schedule_input['value']
                class_type = schedule_input['alt']
            else:
                is_enrolled = 'checked' in enroll_input.attrs
                class_id = enroll_input['value']
                class_type = enroll_input['alt']

            data.add_class(subject_id, class_type, class_id)

            if is_enrolled and data.get_enrolled_class(subject_id, class_type) is None:
                data.enroll(subject_id, class_type, class_id)
            

    # Update soup
    for table in tables:
        for row in table.find_all('tr')[1:]:
            enroll_input = row.find_all('td')[5].find('input')
            if enroll_input is None:
                continue

            class_id = enroll_input['value']
            class_type = enroll_input['alt']

            enrolled_class_id = data.get_enrolled_class(subject_id, class_type)
            if enrolled_class_id is None:
                continue

            if class_id == enrolled_class_id:
                enroll_input['checked'] = 'checked'
            else:
                enroll_input.attrs.pop('checked', None)

    return soup   

def tamper_class_soup(soup):
    if (soup.find('form', {'id': 'inscreverFormBean'}) is None or soup.find('input', {'id': 'botaoGravar'}) is not None):
        return soup

    tables = soup.find('form', {'id': 'inscreverFormBean'}).findAll('table', {'class': 'displaytable'})

    for table in tables:
        new_row_header = soup.new_tag('th')
        new_row_header.string = 'Inscrever'
        new_row_header['class'] = 'cellheader'

        table.find('tr').append(new_row_header)

        for row in table.find_all('tr')[1:]:
            new_row = soup.new_tag('td')
            new_row['class'] = 'contentCenter'
            new_row['style'] = 'width: 10%'

            schedule_input = row.find('input')

            if row.find_all('td')[3].text != '0' or schedule_input.has_attr('disabled'):   
                new_row_input = soup.new_tag('input')
                new_row_input['type'] = 'checkbox'
                new_row_input['title'] = 'Inscrever'
                new_row_input['id'] = 'inscrever{}'.format(schedule_input['value'])
                new_row_input['name'] = 'inscrever'
                new_row_input['alt'] = schedule_input['alt']
                new_row_input['value'] = schedule_input['value']
                new_row_input['onclick'] = 'javascript: checkInscricao(\'{}\', \'{}\');'.format(schedule_input['value'], schedule_input['alt'])

                if schedule_input.has_attr('disabled'):
                    new_row_input['checked'] = 'checked'

                new_row.append(new_row_input)
            else:
                new_row_span = soup.new_tag('span')
                new_row_span['class'] = 'remember'
                new_row_span.string = 'Sem vagas'
                new_row.append(new_row_span)

            row.append(new_row)
            
            schedule_input.attrs.pop('disabled', None)

    submit_span = soup.new_tag('span')
    submit_span['id'] = 'spanSubmitTag'
    submit_span['style'] = 'display:none;'
    
    submit_img = soup.new_tag('img')
    submit_img['src'] = '../images/progressIndicator.svg'
    submit_img['align'] = 'absmiddle'
    submit_span.append(submit_img)
    submit_span.append('&nbsp;&nbsp;')

    submit_span1 = soup.new_tag('span')
    submit_span1['id'] = 'spanSubmitTagMensagem1'
    submit_span1['style'] = 'display:none;'
    submit_span1.string = 'A processar. Por favor aguarde.'
    submit_span.append(submit_span1)

    submit_span2 = soup.new_tag('span')
    submit_span2['id'] = 'spanSubmitTagMensagem2'
    submit_span2['style'] = 'display:none;'
    submit_span2.string = 'A processar.. Por favor aguarde mais alguns instantes.'
    submit_span.append(submit_span2)

    button_input = soup.new_tag('input')
    button_input['type'] = 'submit'
    button_input['value'] = 'Gravar'
    button_input['onclick'] = 'escondeBotoesTagLib(this);'
    button_input['id'] = 'botaoGravar'
    button_input['class'] = 'button buttonFront'

    div = soup.find('div', {'id': 'processar2'})
    div.insert(0, button_input)
    div.insert(0, submit_span)

    return soup

def get():
    args = request.values
    method = args.get('method', '').lower()

    original_request = utils.get_original_request()

    soup = BeautifulSoup(original_request.data, 'html.parser')

    if method == '':
        soup = tamper_enroll_info(tamper_class_soup(soup))
    elif method == 'actualizadados':
        current_timestamp = args.get('_', None)

        timestamp_start = args.get('start', None)
        timestamp_end = args.get('end', None)

        if timestamp_start is None or timestamp_end is None or current_timestamp is None:
            return Response(status=400)
        
        print("Data request from {} to {} at {}".format(timestamp_start, timestamp_end, current_timestamp))
     
    original_request.data = str(soup)
    original_request.headers['Content-Length'] = len(original_request.data)

    return original_request

def on_submit(args):
    referer = request.headers.get('Referer')
    subject_arg = referer.split('=')[1]

    subject_id = data.get_subject_id(subject_arg)
    
    print("Blocked submit request for enrollement in classes of the subject id {}:".format(subject_id))

    for class_id in args.getlist('inscrever'):
        class_type = data.get_class_type(subject_id, class_id)
        if class_type is None:
            print("\tClass {} not found".format(class_id))
            continue

        data.enroll(subject_id, class_type, class_id)

        print("\tEnrolled in class {} of type {}".format(class_id, class_type))

    headers = {'Location': utils.get_client_url('/nonio/inscturmas/listaInscricoes.do')}
    return Response(status=302, headers=headers)

def on_actualizadados(args):
    current_timestamp = args.get('_', None)

    timestamp_start = args.get('start', None)
    timestamp_end = args.get('end', None)

    if timestamp_start is None or timestamp_end is None or current_timestamp is None:
        return Response(status=400)
    
    print("Data request from {} to {} at {}".format(timestamp_start, timestamp_end, current_timestamp))

    return utils.get_original_request()

def post():
    args = request.values
    
    method = args.get('method', '').lower()
    if method == 'submeter':
        return on_submit(args)
    elif method == 'actualizadados':
        return on_actualizadados(args)
    
    return utils.get_original_request()