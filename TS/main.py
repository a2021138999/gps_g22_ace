# pip install flask
import os
import data
import utils
import importlib
from flask import Flask, request, session

app = Flask(__name__)

app.secret_key = os.urandom(24)

@app.before_request
def request_filter():
    if not session.get('initialized'):
        session['initialized'] = True
        data.initialize_session()

    path_components = request.path.strip('/').split('/')
    
    file_path = os.path.join('hooks', *path_components[:-1], '{}.py'.format(path_components[-1]))

    if (not os.path.exists(file_path)):
        return utils.get_original_request()
        
    spec = importlib.util.spec_from_file_location(name=path_components[-1], location=file_path)
    module = importlib.util.module_from_spec(spec)
    spec.loader.exec_module(module)

    function_name = request.method.lower()

    if (not hasattr(module, function_name)):
        print("Method {} not found in {}".format(function_name, file_path))
        return utils.get_original_request()
    
    return getattr(module, function_name)()