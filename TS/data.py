from flask import session

def initialize_session():
    session['subject_id'] = {}
    session['subject_class'] = {}
    session['subject_enrolled'] = {}
    session.modified = True

def get_subject_id(subject_arg):
    return session.get('subject_id', {}).get(subject_arg, subject_arg)

def add_subject_id(subject_arg, subject_id):
    session['subject_id'][subject_arg] = subject_id
    session.modified = True

def add_class(subject_id, class_type, class_id):
    if subject_id not in session['subject_class']:
        session['subject_class'][subject_id] = {}

    session['subject_class'][subject_id][class_type] = class_id
    session.modified = True

def get_class_type(subject_id, class_id):
    subject_classes = session.get('subject_class', {}).get(subject_id, {})

    for class_type in subject_classes:
        if class_id in subject_classes[class_type]:
            return class_type
    
    return None

def enroll(subject_id, class_type, class_id):
    if subject_id not in session['subject_enrolled']:
        session['subject_enrolled'][subject_id] = {}

    session['subject_enrolled'][subject_id][class_type] = class_id
    session.modified = True

def get_enrolled_class(subject_id, class_type):
    return session.get('subject_enrolled', {}).get(subject_id, {}).get(class_type, None)