# ACE - Automated Class Enroller

## Contents

- [Team](#team)
- [Vision and Scope](#vision-and-scope)
- [Requirements](#requirements)
  - [Mockups](#mockups)
  - [User stories](#user-stories)
- [Definition of Done](#definition-of-done)
- [Architecture and Design](#architecture-and-design)
  - [Domain Model](#domain-model)
- [Risk Plan](#risk-plan)
- [Pre-Game](#pre-game)
- [Release Plan](#release-plan)
  - [Release 1](#release-1)
  - [Release 2](#release-2)
- [Increments](#increments)
  - [Sprint 1](#sprint-1)
  - [Sprint 2](#sprint-2)
  - [Sprint 3](#sprint-3)

## Team

- Daniela Correia - a2021143404@isec.pt
- Pedro Rodrigues Jorge - a2021142041@isec.pt
- Mateus Oliveira - a2021136689@isec.pt
- Tiago Cardoso - a2021138999@isec.pt
- João Neves - a2021133564@isec.pt

---

## Vision and Scope

#### Problem Statement

The school's current enrollment system faces issues such as a shortage of slots and intense competition for class registrations.

##### Project background

To address these issues and improve student satisfaction, we aim to develop an application for automatic class registration on the school's website.

##### Stakeholders

- Students
- School Administration

##### Users

- Students
  - Needs: Students desire a more accessible, efficient, and user-friendly class scheduling system that allows them to create schedules quickly and easily, as well as receive notifications about the availability of slots.

---

#### Vision & Scope of the Solution

##### Vision statement

Our project's vision is to build an automated class registration system that provides equitable access to classes, ensuring that every student can secure the courses they need without the frustration of registration challenges.

##### List of features

Efficient Class Scheduling:

    Students can easily create customized class schedules that accommodate their individual preferences and academic requirements.

Pre-enrollment Automation:

    Simplification of pre-enrollment processes to reduce administrative burden, making it possible for students to enroll in different classes in a much more efficient way.

User-friendly Interface:

    A user-friendly graphical interface that ensures students can navigate and interact with the system being developed easily.

Automatic availability checker:

    Students can save their specific information and details for specific classes that are currently full, and the system will automatically enroll them if slots become - available when they login into platform.

##### Features that will not be developed

Notification of Class Slot Availability:

    The system will notify students about the real-time availability of class slots, ensuring that they are informed about openings.

Priority Setting:

     The user specifies their priorities, and the system automatically creates the schedule based on the student's availability.

---

## Requirements

### Mockups

![Alt text](mockups/main_page.png)
![Alt text](mockups/welcom_page.png)
![Alt text](mockups/subjectList_page.png)
![Alt text](mockups/classesList_page.png)
![Alt text](mockups/enrollPopUp.png)
![Alt text](mockups/preferences_search.png)
---

## Definition of done

(This section is already written, do not edit)
It is a collection of criteria that must be completed for a User Story to be considered “done.”

1. All tasks done:

- CI – built, tested (Junit), reviewed (SonarCloud)
- Merge request to qa (code review)

2. Acceptance tests passed
3. Accepted by the client
4. Code merged to main

---

### User Stories (With MoSCoW priorities method)

##### User Story 1: List Subjects from external platform (Must Have) (Story Points: M) - done .. 

- As a student, I want to be able to see all the subjects from Nonio (Inforestudante) listed (of the year and semester that I'm in) so I can have an idea of the subjects/classes available.

| Acceptance Criteria (US1) | Description                                                                                                  |
| ------------------------- | ------------------------------------------------------------------------------------------------------------ |
| Subject Listing           | When I access the platform, there should be a section or page that lists all the available subjects.         |
| Subject Information       | For each subject listed, I should be able to see the information (Subject name, Subject classes information) |
| User Interface            | Should have a UI to see all details needed                                                                   |

##### User Story 2: Enroll into a subject (Must Have) (Story Points: L) - done ..

- As a student, I want to select a specific subject so I can enroll into it.

| Acceptance Criteria (US2) | Description                                                                                                                         |
| ------------------------- | ----------------------------------------------------------------------------------------------------------------------------------- |
| Subject Selection         | I should be able to browse the list of subjects and select a specific subject that I want to enroll into                            |
| Enrollment Button         | When I select a subject, there should be a clear and easily identifiable "Enroll" or "Register" button associated with that subject |
| Confirmation              | After clicking the "Enroll" button, I should receive a confirmation message that confirms my enrollment                             |
| User Interface            | Should have a UI to see all details needed                                                                                          |

##### User Story 3: Preferences (Should Have) (Story Points: L)

- As a student, I want the ability to set my preferences so I can choose my preferred class timings and avoid specific time slots that are inconvenient for me.

| Acceptance Criteria (US3) | Description                                                                                            |
| ------------------------- | ------------------------------------------------------------------------------------------------------ |
| Day Preferences           | Specify days of the week when I prefer to have classes and days when I prefer not to have classes      |
| Class Types               | If there are different class types (e.g., P, T, TP), I should be able to set preferences for each type |
| Enrollment Priority       | The system should consider my preferences when assigning me to classes                                 |
| User Interface            | Should have a UI to see all details needed                                                             |

##### User Story 4: Automatic Availability Checker (Could Have) (Story Points: S)

- As a student, I want the pre-enrollment process to be "automated" so I can quickly and easily enroll in my desired classes without administrative hassles.

Acceptance Criteria (US4) -> haven't thought of it yet.

##### User Story 5: Subject Search and Selection (Must Have) (Story Points: L)

- As a student, I want the system to provide an efficient Subject search feature so I can browse available subject, view details, and select my desired classes with ease.

| Acceptance Criteria (US5) | Description                                                                                                                |
| ------------------------- | -------------------------------------------------------------------------------------------------------------------------- |
| Search Functionality      | I should have access to a search feature within the system to search for available subjects                                |
| Keyword Search            | The search feature should support keyword-based searches, allowing me to search by subject name, code, or related keywords |
| Filtering Options         | The system should provide filtering options to narrow down subject search results based on criteria                        |
| User Interface            | Should have a UI to see all details needed                                                                                 |

##### User Story 6: Pre-Requisite Checking (Should Have) (Story Points: S) - done ..

- As a student, I want the system to automatically check course prerequisites and inform me if I'm eligible to enroll, so I can only see courses that I can register for.

| Acceptance Criteria (US6) | Description                                                                                                                                  |
| ------------------------- | -------------------------------------------------------------------------------------------------------------------------------------------- |
| Eligibility Status        | The system should determine and display my eligibility status for enrolling in a course based on the prerequisite checking                   |
| Conditional Enrollment    | If I don't fully meet the prerequisites but am still eligible to enroll under certain conditions,the system guide me through necessary steps |
| User Interface            | Should have a UI to see all details needed                                                                                                   |

##### User Story 7: Customizable Schedule (Could Have) (Story Points: S)

- As a student, I want the option to create a customizable class schedule within the system, so I can plan my courses in a way that suits my preferences and other commitments.

Acceptance Criteria (US7) 
  - Conflict Restrictions: The system should alert students about schedule conflicts between classes, avoiding overlapping or conflicting time slots.
  - Allow students to input courses based on their available time, with the ability to view and adjust the schedule on a weekly basis.

##### User Story 8: Subject Recommendation (Won't have this time) (Story Points: S)

- As a student, I would like to be recommended to attend classes that align with my extracurricular activities so I can minimize the time spent searching for the most suitable class, even though this feature won't be implemented at this time

Acceptance Criteria (US8) -> haven't thought of it yet.

##### User Story 9: Subject Notification service (Won't have this time) (Story Points: M)

- As a student, I would like the system to notify me of any last-minute changes or updates to recommended courses, such as changes in class times or cancellations, so I can make adjustments to my schedule in a timely manner and avoid conflicts.

Acceptance Criteria (US9) -> haven't thought of it yet.

##### User Story 10: Login (Must Have) (Story Points: S) - done ..

- As a student, I want to be able to log in with my university's credentials so I can access the application securely and conveniently.

| Acceptance Criteria (US10) | Description                                                                                                                  |
| -------------------------- | ---------------------------------------------------------------------------------------------------------------------------- |
| University Credentials     | Students should be able to log in using their university-issued credentials, such as a username and password                 |
| Session Management         | Implement session management to keep users logged in until they choose to log out or their session expires due to inactivity |
| User Interface             | Should have a UI to see all details needed                                                                                   |

---

## Architecture and Design

#### Domain Model

![Alt text](domainModel.png)

---

## Risk Plan

##### Threshold of Success

- By the 1st release date, we should be able to login and enroll in a subject.
- By the 2nd release date, we should be able to give our preferences and enroll in all of our subjects, according to possible limitations, such as the number of available slots.
- The program should be able to function for future iterations of the nonio platform (for example for the second semester)

##### Risk List

- RSK1 - We are completely dependent on external software, which means that if the external software changes the way data is presented, our software may also needs to be altered
  being left with limited test flexibility.
- RSK2 - The NONIO platform can change when the enrolments reopen next semester (or any reopening in another time) and we have no way of future-profing our ACE project for future iterations of the enrolment process.
- RSK3 - Both the test's and the nonio's server could be overloaded with all of the requests, not being able to respond correctly to our needs.

##### Mitigation Actions (threats>=20)

- RSK1 - Create a test class where artificial information is injected into the API to test the desired output.
- RSK2 - Adapt the ACE project to work with the new iteration of the enrolment process of NONIO.
- RSK3 - Adapt our application to queue the requests that are made so that they are not lost during the problems with the connection and can resume when the servers are available again

---

##### Assumptions

- It is guaranteed that the user's familiarity with the operation of technological devices is a fundamental basis for interacting correctly with any application. When users have a solid understanding of how to use devices, they are better able to make the most of the features and resources of an application.

- It is ensured that the user trusts the application and that it will have strong and dedicated security measures for its use.

- It is guaranteed that the user is familiar with the operation of class schedules

---

## Pre-Game

### Sprint 0 Plan

- Goal: Prepare project (details / needs)
- Dates: from 12/Oct to 26/Oct, 2 weeks
- Sprint 0 Backlog (don't edit this list):
  - Task1 – Write Team
  - Task2 – Write V&S
  - Task3 – Write Requirements
  - Task4 – Write DoD
  - Task5 – Write Architecture&Design
  - Task6 – Write Risk Plan
  - Task7 – Write Pre-Gane
  - Task8 – Write Release Plan
  - Task9 – Write Product Increments
  - Task10 – Create Product Board
  - Task11 – Create Sprint 0 Board
  - Task12 – Write US in PB, estimate (SML), prioritize (MoSCoW), sort
  - Task13 – Create repository with “GPS Git” Workflow

---

## Release Plan

### Release 1

- Goal: MVP - Program to Enroll in a specified Class
- Date: 23/Nov
- Release: V1.0
- Team Capacity/Budget: 4 x 5 x 4 = 80 (Hours) -> ( Hours/week | Team members | number of weeks)

- Functionalities proposed by the client:

  | Preview Date       | Functionality                                                                |
  | ------------------ | ---------------------------------------------------------------------------- |
  | 9th November 2023  | The user (student) should see all subjects listed into a UI                  |
  | 9th November 2023  | The user (student) should be able to login with the University's Credentials |
  | 23th November 2023 | The user (student) should be able to enroll into a selected subject          |

---

### Release 2

- Goal: Final release – Program to Enroll in classes for all subjects with specified preferences
- Date: 14/Dec
- Release: V2.0

---

## Increments

### Sprint 1

##### Sprint Plan

- Goal: Do autheticantion login feature and list all subjects associated to our university's account (enrolled and not enrolled)

- Dates: from 26/Oct to 09/Nov, 2 weeks

- Roles:

  - Product Owner: Mateus
  - Scrum Master: Pedro

- Note: "external platform" = (eg. Inforestudante)

- To do:

  - US1: As a student, I want to be able to see all the subjects from Nonio (Inforestudante) listed, so I can create a idea of subjects possibilities.

    | TasksUS1 | Functionality                                            |
    | -------- | -------------------------------------------------------- |
    | Task1    | List all subjects into plataform                         |
    | Task2    | Receive subjects information from external plataform     |
    | Task3    | Get data/hours of subject classes from external platform |
    | Task4    | Create a UI to see the list of all subjects              |

  - US10: As a student, I want to be able to log in with my university's credentials so I can access the application securely and conveniently.

    | TasksUS2 | Functionality                      |
    | -------- | ---------------------------------- |
    | Task1    | Login with university's credential |
    | Task2    | Create a UI to a Login Page        |

  - Development: We need to create a clone of Inforestudante in order to have a test environment

    - Task: Create a clone of Inforestudante (Enrollment Page)

- Story Points: 2M + (aprox. 12 hours to create a Development Task)
- Total time spent: aprox. 26 hours
- Max. time to be spent: 30 hours

- Analysis: We need to create a clone of Inforestudante in order to have a test environment. The program will show up a login page that will, in case of success, redirect to the main page with all the available tweaks (for automatic enrollment) and subjects (for manual enrollment).

##### Sprint Review

- Analysis: In terms of features, the team managed to implement all of the proposed features for the sprint.
			The management and organization of the sprint started out rough but was improved day upon day to fit the model established by the client, finally getting to a result that satisfied the client.
- Story Points: 2M + 12 hours for development tasks. The total time spent were 26 hours
- Version: 1.0
- Client analysis: The client accepted our release in its totality with very few changes to be made (both having to do with event transparency to the user). We concluded that the client was satisfied with the delivered result.
- Conclusions: We, as a team, concluded that the sprint was in par with our expectations and propositions to the client, fully delivering what was promised and with only a couple changes to be made.

##### Sprint Retrospective

- What we did well:
  - All the proposed features were implemented
  - The work-load was very organized inside the team, with members focusing on specific tasks both in the coding part (implementing the features) and the management part of the sprint (updating issue boards, rewriting and updating parts of the README and other gitlab funcionalities such as configuring the Milestone)
- What we did less well:
  - The estimations were off even though they almost canceled out in the end
  - Little confusions about how we should organize the plan for the sprint on the issue boards (even though they were fixed when the sprint ended)
- How to improve to the next sprint:
  - Use the real time spent on Sprint 1's US and tasks to help us create more accurate estimations for Sprint 2's US and tasks
  - Use the already-established model of the gitlab project's management and organization as base model to organize Sprint 2

---

#### Sprint 2

##### Sprint Plan

- Goal: Created a class enrollment system throw platform

- Dates: from  09/Nov to 23/Nov, 2 weeks

- Roles:

  - Product Owner: Daniela
  - Scrum Master: Tiago

- To do:

  - US2: As a student, I want to select a specific subject so I can enroll into it.

    | TasksUS2 | Functionality                                                                                       |
    | -------- | --------------------------------------------------------------------------------------------------  |
    | USTask1  | Implement the enrollment process for the selected subject                                           |
    | USTask2  | Class Selection, Enable the user to select a specific subject from the list in UI.                  |
    | USTask3  | Conduct thorough testing to verify that subject selection and enrollment processes work correctly   |
    | USTask4  | Provide a confirmation message or screen after a successful enrollment                              |

  - US6: As a student, I want the system to automatically check course prerequisites and inform me if I'm eligible to enroll, so I can only see courses that I can register for.

    | TasksUS6   | Functionality                                                                                                     |
    | ---------- | ----------------------------------------------------------------------------------------------------------------- |
    | USTask1    | Ensure that the system provides detailed information about the prerequisites for each course                      |
    | USTask2    | Implement a notification mechanism to inform the student whether they meet the prerequisites for a specific course|

  - Fixes and configuring/adding tasks
    - Task: Develop an interactive loading status indicator to keep users informed while the program executes background services.
    - Task: Configure pipeline to safeguard personal data and automate testing processes for enhanced security and reliability.
    - Task: Implement targeted error messaging, such as providing specific notifications for login errors in order to enhance user understandment and to provide a more user-friendly experience.

- Story Points: 1L + 1S + (aprox. 8.5 hours spent into fixes and configuring the sprint's issue boards and other activities)
- Max. time to be spent: 30 hours

##### Sprint Review

- Analysis: The team managed to implement all of the proposed features for the sprint
- Story Points: 1L + 1S hours for development tasks. The total time spent was 37 hours and 59 minutes, compared to the 32 hours and 45 minutes estimated
- Version: 1.1
- Client analysis: The client was satisfied with the delivered product and accepted our sprint
- Conclusions: We, as a team, concluded that the sprint was in par with our expectations and propositions to the client, delivering what was promised.

##### Sprint Retrospective

- What we did well:
  - All the proposed features were implemented
  - The work-load was very organized inside the team, with members focusing on specific tasks both in the coding part (implementing the features) and the management part of the sprint (updating issue boards, rewriting and updating parts of the README and other gitlab funcionalities such as configuring the Milestones)
  - The demo presented to the client was in par with the expectations 
- What we did less well:
  - The estimates were still not very close to the real time spent
- How to improve to the next sprint:
  - Improve our estimations, using the sprint 2's task's real time spent as a comparison

#### Sprint 3

##### Wiki week 13 
- https://gitlab.com/a2021138999/gps_g22_ace/-/wikis/Meeting-Checklist-(Week-13)
##### Sprint Plan

- Goal: Create functionality to select Subjects with specified preferences and enroll in it

- Dates: from  23/Nov to 14/Dez, 3 weeks

- Roles:

  - Product Owner: Pedro Jorge
  - Scrum Master: João Neves

- To do:

  - US5 : As a student, I want the system to provide an efficient Subject search feature so I can browse available subject, view details, and select my desired classes with ease
      - USTask: Create a top menu with preferences options
      - USTask: Create a user preferences filter UI
      - USTask: Create Algorithm to browse preferences on a specific server (Flask server/ipc)

  - US3: As a student, I want the ability to set my preferences so I can choose my preferred class timings and avoid specific time slots that are inconvenient for me.
      - USTask:  Create a preference's search result UI
      - USTask:  Create UI to list my preferences
      - USTask:  Create a functionality to save preferences (Serialization)
      - USTask:  Create a functionality to restore preferences (Serialization)
      
  - US7: As a student, I want the option to create a customizable class schedule within the system, so I can plan my classes in a way that suits my preferences and other commitments
      - USTask: Create a method to show current search effects as a support
      - USTask: Create a List pane to see efforts

 - Fixes and configuring/adding tasks
    - Task: Update TestServer to handle with efforts 

- Story Points: 2L + 1S + 16 hours (by confirguration task)
- Max. time to be spent: 50 hours (including USTasks and Config Tasks)

##### Sprint Review

- Analysis: The team managed to implement all of the proposed features for this last sprint
- Story Points: 2L + 1S + 16 hours for configuration tasks. The total time spent was 53 hours and 1 minute, compared to the 50 hours and 35 minutes estimated
- Version: 2.0
- Client analysis: The client was satisfied with the delivered product and accepted our final release
- Conclusions: We, as a team, concluded that the sprint was in par with our expectations and propositions to the client, delivering what was promised.

##### Sprint Retrospective

- What we did well:
  - All of the proposed features were implemented
  - The management process was seamless, the team managed to keep control of the product and the sprint until the very end
  - The demo presented for this release was in par with our expectations as well as the client's
  
---