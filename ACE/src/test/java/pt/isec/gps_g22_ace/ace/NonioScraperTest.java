package pt.isec.gps_g22_ace.ace;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;

import static org.junit.jupiter.api.Assertions.*;

import pt.isec.gps_g22_ace.ace.data.Class;
import pt.isec.gps_g22_ace.ace.data.Subject;
import pt.isec.gps_g22_ace.ace.data.Registration;

import java.awt.image.BufferedImage;
import java.util.List;

public class NonioScraperTest
{
    /*
    private final NonioScraper scraper = new NonioScraper("http://127.0.0.1:5000");
    private final String email = System.getProperty("email");
    private final String password = System.getProperty("password");

    @BeforeEach
    public void setUp() throws Exceptions.UserAlreadyLoggedIn, Exceptions.Warning, Exceptions.InvalidStatus, Exceptions.UserNotLoggedIn
    {
        if (email == null || password == null || email.isEmpty() || password.isEmpty())
            throw new RuntimeException("Email and password must be provided as system properties");

        assertFalse(scraper.isLogged());

        scraper.login(email, password, null);

        assertTrue(scraper.isLogged());
        System.out.println("Logged in as " + scraper.getName());
    }

    @Test
    public void getCaptchaImage() throws Exceptions.Warning {
        BufferedImage image = scraper.getCaptchaImage();

        assertNotNull(image);
    }

    @Test
    public void getName() throws Exceptions.UserNotLoggedIn, Exceptions.Warning, Exceptions.InvalidStatus {
        String name = scraper.getName();

        assertNotNull(name);
        assertFalse(name.isEmpty());
    }

    @Test
    public void getRegistrations() throws Exceptions.UserNotLoggedIn, Exceptions.InvalidStatus, Exceptions.Warning {
        List<Registration> registrations = scraper.getRegistrations();

        assertNotNull(registrations);
        assertFalse(registrations.isEmpty());
    }

    @Test
    public void getSubjects() throws Exceptions.UserNotLoggedIn, Exceptions.InvalidStatus, Exceptions.Warning {
        List<Registration> registrations = scraper.getRegistrations();

        assertNotNull(registrations);
        assertFalse(registrations.isEmpty());

        for (Registration registration : registrations)
        {
            List<Subject> subjects = registration.getSubjects();

            assertNotNull(subjects);
            assertFalse(subjects.isEmpty());
        }
    }

    @Test
    public void getClasses() throws Exceptions.UserNotLoggedIn, Exceptions.InvalidStatus, Exceptions.Warning {
        List<Registration> registrations = scraper.getRegistrations();

        assertNotNull(registrations);
        assertFalse(registrations.isEmpty());

        for (Registration registration : registrations)
        {
            List<Subject> subjects = registration.getSubjects();

            assertNotNull(subjects);
            assertFalse(subjects.isEmpty());

            for (Subject subject : subjects)
            {
                List<Class> classes = subject.getClasses();

                if (classes == null)
                    continue;

                if (subject.canEnroll())
                    assertFalse(classes.isEmpty());
                else
                    assertTrue(classes.isEmpty());
            }
        }
    }

    @Test
    public void enroll() throws Exceptions.UserNotLoggedIn, Exceptions.InvalidStatus, Exceptions.Warning {

        assertTrue(scraper.isLogged());
        List<Registration> registrations = scraper.getRegistrations();

        assertNotNull(registrations);
        assertFalse(registrations.isEmpty());

        for (Registration registration : registrations)
        {
            List<Subject> subjects = registration.getSubjects();

            assertNotNull(subjects);
            assertFalse(subjects.isEmpty());

            for (Subject subject : subjects)
            {
                List<Class> classes = subject.getClasses();
                if (classes == null)
                    continue;

                if (!subject.canEnroll())
                {
                    assertTrue(classes.isEmpty());
                    continue;
                }

                assertFalse(classes.isEmpty());

                for (Class class_ : classes)
                {
                    if (!class_.canEnroll())
                        continue;

                    assertTrue(subject.enroll(class_));
                }

                assertTrue(subject.processEnroll());
            }
        }
    }

    @Test
    public void getSchedules() throws Exceptions.UserNotLoggedIn, Exceptions.InvalidStatus, Exceptions.Warning {
        List<Registration> registrations = scraper.getRegistrations();

        assertNotNull(registrations);
        assertFalse(registrations.isEmpty());

        for (Registration registration : registrations)
        {
            List<Subject> subjects = registration.getSubjects();

            assertNotNull(subjects);
            assertFalse(subjects.isEmpty());

            for (Subject subject : subjects)
            {
                List<Class> classes = subject.getClasses();

                if (classes == null)
                    continue;

                if (!subject.canEnroll())
                {
                    assertTrue(classes.isEmpty());
                    continue;
                }

                assertFalse(classes.isEmpty());

                for (Class class_ : classes)
                {
                    assertNotNull(class_.getSchedule());

                    System.out.println("Class " + class_.getName() + " schedule: " + class_.getSchedule());
                }
            }
        }
    }

    @AfterEach
    public void tearDown() throws Exceptions.UserNotLoggedIn, Exceptions.InvalidStatus, Exceptions.Warning {
        assertTrue(scraper.isLogged());

        scraper.logout();

        assertFalse(scraper.isLogged());
    }*/
}