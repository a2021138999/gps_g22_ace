module com.example.ace {
    requires javafx.controls;
    requires javafx.fxml;

    requires org.kordamp.bootstrapfx.core;
    requires org.jsoup;
    requires java.desktop;
    requires org.json;

    opens pt.isec.gps_g22_ace to javafx.fxml;
    exports pt.isec.gps_g22_ace;
    exports pt.isec.gps_g22_ace.ace;
    exports pt.isec.gps_g22_ace.utils;
    exports pt.isec.gps_g22_ace.ace.data;
    exports pt.isec.gps_g22_ace.Controllers;
    opens pt.isec.gps_g22_ace.Controllers to javafx.fxml;

}