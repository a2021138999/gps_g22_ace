package pt.isec.gps_g22_ace.Controllers;

import javafx.application.Platform;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.collections.ObservableMap;
import javafx.event.ActionEvent;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.control.Label;
import javafx.scene.control.ListView;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.scene.text.Font;
import javafx.scene.text.FontPosture;
import javafx.stage.Screen;
import pt.isec.gps_g22_ace.App;
import pt.isec.gps_g22_ace.ace.ACE;
import pt.isec.gps_g22_ace.ace.data.Class;
import pt.isec.gps_g22_ace.ace.data.Subject;
import pt.isec.gps_g22_ace.utils.Loader;

import java.io.IOException;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Objects;

public class ResultsController
{
    public ListView<HBox> resultList;
    public VBox rootPane;
    private static ObservableMap<Subject, ObservableList<Class>> classMap;

    public static void setResults(ObservableMap<Subject, ObservableList<Class>> classes)
    {
        ResultsController.classMap = classes;
    }

    public void initialize()
    {
        ObservableList<HBox> cardList = FXCollections.observableArrayList();

        if (classMap.isEmpty())
        {
            HBox card = FailCard();
            cardList.add(card);
        }
        else
        {
            for (Subject subject : classMap.keySet())
            {
                ObservableList<Class> classes = classMap.get(subject);

                for (Class aClass : classes)
                    cardList.add(createCard(subject.getName(), aClass));
            }
        }

        resultList.setItems(cardList);
    }

    private HBox createCard(String s, Class c)
    {
        HBox card = new HBox(20); // Espaçamento entre os elementos do card
        card.getStyleClass().add("card"); // Aplicar o estilo CSS aos cartões
        card.setStyle(
                "-fx-background-color:  #2f2d2d;" + // Cor de fundo branca
                        "-fx-text-fill: white;" + // Cor do texto
                        "-fx-border-color: #212121;" + // Cor da borda
                        "-fx-border-width: 2px;" +
                        "-fx-border-radius: 0px;" + // Borda quadrada
                        "-fx-padding: 10px;" // Espaçamento interno
        );

        Label classLabel = new Label("Subject: " + s + "\n");
        Label nameLabel = new Label("Class: " + c.getName() + "\n");
        Label teacherLabel = new Label("Teacher: " + c.getTeacherName() + "\n");

        Label timings = new Label("Day: "+c.getSchedule().getDayOfWeek()
                +"\tStart: " + c.getSchedule().getStartTime()
                + "\t End: " + c.getSchedule().getEndTime());

        nameLabel.getStyleClass().add("label");
        teacherLabel.getStyleClass().add("label");
        double fontSize = 0.02 * Screen.getPrimary().getVisualBounds().getWidth();
        Font proportionalFont = Font.font("Arial", FontPosture.REGULAR, fontSize);

        nameLabel.setFont(proportionalFont);
        teacherLabel.setFont(proportionalFont);

        card.getChildren().addAll(new VBox(classLabel,nameLabel, teacherLabel, timings));

        return card;
    }

    private HBox FailCard()
    {
        HBox card = new HBox(20); // Espaçamento entre os elementos do card
        card.getStyleClass().add("card"); // Aplicar o estilo CSS aos cartões
        card.setStyle(
                "-fx-background-color:  #2f2d2d;" + // Cor de fundo branca
                        "-fx-text-fill: red;" + // Cor do texto
                        "-fx-border-color: #212121;" + // Cor da borda
                        "-fx-border-width: 2px;" +
                        "-fx-border-radius: 0px;" + // Borda quadrada
                        "-fx-padding: 10px;" // Espaçamento interno
        );

        Label classLabel = new Label("Any Result Found");
        classLabel.setStyle("-fx-text-fill: red");

        return card;
    }

    public void onBackButtonClick(ActionEvent actionEvent)
    {
        try
        {
            Parent node = FXMLLoader.load(Objects.requireNonNull(App.class.getResource("subject-list-view.fxml")));
            rootPane.getScene().setRoot(node);
        }
        catch (IOException e)
        {
            throw new RuntimeException(e);
        }
    }

    public void onProcessEnrollInResult(ActionEvent actionEvent) {
        List<String> response = new ArrayList<>();
        Loader.getInstance().runProcessWithLoading(rootPane, () ->
        {
            for (Subject subject : classMap.keySet())
            {
                ObservableList<Class> classes = classMap.get(subject);

                for(Class c : classes){
                    if(!subject.enroll(c)){
                        response.add("Atualmente inscrito na turma "+c.getName()+" - " +subject.getName() +"");
                        break;
                    }else{
                        response.add("Inscrito com sucesso na turma "+c.getName()+" - " + subject.getName() + "");
                    }
                }

                subject.processEnroll();

            }

            Platform.runLater(() ->
            {
                EffortsController.setResults(response);
                try
                {
                    Parent node = FXMLLoader.load(Objects.requireNonNull(App.class.getResource("efforts-view.fxml")));
                    rootPane.getScene().setRoot(node);
                } catch (IOException e)
                {
                    throw new RuntimeException(e);
                }
            });
        });
    }
}
