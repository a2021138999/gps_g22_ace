package pt.isec.gps_g22_ace.Controllers;

import javafx.application.Platform;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.collections.ObservableMap;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.geometry.Pos;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.control.Label;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.VBox;
import javafx.scene.paint.Color;
import pt.isec.gps_g22_ace.App;
import pt.isec.gps_g22_ace.ace.ACE;
import pt.isec.gps_g22_ace.ace.NonioScraper;
import pt.isec.gps_g22_ace.ace.data.*;

import java.io.IOException;
import java.time.LocalTime;
import java.time.format.TextStyle;
import java.util.*;

import javafx.scene.control.ChoiceBox;
import pt.isec.gps_g22_ace.ace.data.Class;
import pt.isec.gps_g22_ace.utils.Loader;

import java.time.DayOfWeek;

public class PreferencesController
{
    @FXML
    public VBox rootPane;
    @FXML
    public Label title;
    @FXML
    public GridPane calendarGrid;
    @FXML
    public Label error;

    @FXML
    private ChoiceBox<String> selectBox;

    private final static List<Schedule> selectedHours = new ArrayList<>();

    public void initialize()
    {
        title.setText("Preferences Search");
        title.setTextFill(Color.WHITE);
        title.setAlignment(Pos.TOP_CENTER);

        selectBox.getItems().addAll("T", "TP", "P", "ALL");
        initializeCalendarGrid();

        this.error.setTextFill(Color.RED);
    }

    private void initializeCalendarGrid()
    {
        for (int i = 1; i < 6; i++)
        {
            Label label = new Label(DayOfWeek.of(i).getDisplayName(TextStyle.FULL, Locale.ENGLISH));
            label.getStyleClass().add("calendar-label");
            calendarGrid.add(label, i, 0);
        }

        for (int i = 1; i < 32; i++)
        {
            Label label = new Label(LocalTime.of(8, 0).plusMinutes(i * 30L).toString());
            label.getStyleClass().add("calendar-label");
            calendarGrid.add(label, 0, i);
        }

        for (int i = 1; i < 6; i++)
        {
            for (int j = 1; j < 32; j++)
            {
                Label label = new Label();
                label.setId(i + "-" + j);
                label.getStyleClass().add("calendar-cell");
                label.setOnMouseClicked((event) -> handleCellClick(label.getId()));
                calendarGrid.add(label, i, j);
            }
        }
    }

    private void handleCellClick(String cellId)
    {
        String[] cellIdSplit = cellId.split("-");
        int col = Integer.parseInt(cellIdSplit[0]);
        int row = Integer.parseInt(cellIdSplit[1]);

        if (col == 0)
            return;

        DayOfWeek dayOfWeek = DayOfWeek.of(col);
        Schedule selectedHour = new Schedule(dayOfWeek, LocalTime.of(8, 0).plusMinutes(row * 30L), 30);

        if (selectedHours.contains(selectedHour))
        {
            selectedHours.remove(selectedHour);
            Node cell = calendarGrid.lookup("#" + cellId);
            cell.getStyleClass().remove("selected-cell");
        }
        else
        {
            selectedHours.add(selectedHour);
            Node cell = calendarGrid.lookup("#" + cellId);
            cell.getStyleClass().add("selected-cell");
        }
    }


    public void onBackButtonClick(ActionEvent actionEvent)
    {
        try
        {
            Parent node = FXMLLoader.load(Objects.requireNonNull(App.class.getResource("subject-list-view.fxml")));
            rootPane.getScene().setRoot(node);
        } catch (IOException e)
        {
            throw new RuntimeException(e);
        }
    }

    public void onSearchButtonClick(ActionEvent actionEvent)
    {
        Loader.getInstance().runProcessWithLoading(rootPane, () ->
        {
            Preferences preferences = new Preferences();
            selectedHours.forEach((schedule) -> preferences.addFilter(new Filter(Filter.Type.EXCLUDE_SCHEDULE, schedule)));

            Map<Subject, List<Class>> searchResult = ACE.getInstance().applyAlgorithm(preferences);
            if (searchResult == null)
            {
                Platform.runLater(() -> this.error.setText("No valid results found"));
                return;
            }

            ObservableMap<Subject, ObservableList<Class>> send = FXCollections.observableHashMap();

            for (Map.Entry<Subject, List<Class>> entry : searchResult.entrySet())
            {
                Subject subject = entry.getKey();
                List<Class> classes = entry.getValue();
                ObservableList<Class> observableList = FXCollections.observableArrayList(classes);
                send.put(subject, observableList);
            }

            Platform.runLater(() ->
            {
                ResultsController.setResults(send);

                try
                {
                    Parent node = FXMLLoader.load(Objects.requireNonNull(App.class.getResource("preferences-result-view.fxml")));
                    rootPane.getScene().setRoot(node);
                }
                catch (IOException e)
                {
                    throw new RuntimeException(e);
                }

                selectedHours.clear();
                calendarGrid.getChildren().forEach((node) -> node.getStyleClass().remove("selected-cell"));
            });
        });
    }
}