package pt.isec.gps_g22_ace.Controllers;

import java.util.Objects;
import java.io.IOException;
import java.util.Stack;

import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.event.ActionEvent;
import javafx.scene.Parent;
import javafx.scene.layout.StackPane;
import javafx.scene.layout.VBox;
import javafx.scene.paint.Color;
import javafx.scene.control.Label;
import javafx.scene.control.Button;
import javafx.application.Platform;
import javafx.scene.control.TextField;
import javafx.scene.control.PasswordField;

import pt.isec.gps_g22_ace.App;
import pt.isec.gps_g22_ace.ace.ACE;
import pt.isec.gps_g22_ace.utils.Loader;

public class LoginController
{
    @FXML
    public VBox rootPane;

    @FXML
    public Label error;
    @FXML
    public TextField domainTextField;
    @FXML
    public TextField emailTextField;
    @FXML
    public PasswordField passwordField;
    @FXML
    public Button btnLogin;

    private final ACE ace = ACE.getInstance();

    public void initialize()
    {
        domainTextField.setText(ace.getDomain());

        Platform.runLater(() -> emailTextField.requestFocus());

        this.error.setTextFill(Color.RED);
    }

    private void setError(String error)
    {
        this.error.setText(error);
    }

    public void onLoginButtonClick(ActionEvent actionEvent)
    {
        Loader.getInstance().runProcessWithLoading(rootPane, () ->
        {
            if (!ace.setDomain(domainTextField.getText()))
            {
                setError(ace.getLastError());
                return;
            }

            if (ace.login(emailTextField.getText(), passwordField.getText()))
            {
                Platform.runLater(() ->
                {
                    try
                    {
                        Parent node = FXMLLoader.load(Objects.requireNonNull(App.class.getResource("registration-list-view.fxml")));
                        rootPane.getScene().setRoot(node);
                    }
                    catch (IOException e)
                    {
                        throw new RuntimeException(e);
                    }
                });
            }
            else
            {
                Platform.runLater(() ->
                {
                    setError(ace.getLastError());
                });
            }
        });
    }
}