package pt.isec.gps_g22_ace.Controllers;

import java.util.Objects;
import java.io.IOException;

import javafx.fxml.FXML;
import javafx.geometry.Pos;
import javafx.fxml.FXMLLoader;
import javafx.event.ActionEvent;
import javafx.scene.Parent;
import javafx.scene.layout.StackPane;
import javafx.scene.layout.VBox;
import javafx.scene.paint.Color;
import javafx.scene.control.Label;
import javafx.scene.control.Button;
import javafx.application.Platform;
import javafx.scene.control.ListView;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;

import pt.isec.gps_g22_ace.App;
import pt.isec.gps_g22_ace.utils.Loader;
import pt.isec.gps_g22_ace.ace.data.Class;
import pt.isec.gps_g22_ace.ace.data.Subject;

public class SubjectListController
{
    @FXML
    public VBox rootPane;

    @FXML
    public Label title;
    @FXML
    private ListView<Button> subjectListView;

    private static ObservableList<Subject> subjects = null;

    public static void setSubjects(ObservableList<Subject> subjects)
    {
        SubjectListController.subjects = subjects;
    }

    public void initialize()
    {
        Loader.getInstance().runProcessWithLoading(rootPane, () ->
        {
            Platform.runLater(() ->
            {
                title.setText("Subject List");
                title.setTextFill(Color.WHITE);
                title.setAlignment(Pos.TOP_CENTER);

                if (subjects == null || subjects.isEmpty())
                    return;

                ObservableList<Button> buttons = FXCollections.observableArrayList();
                for (Subject subject: subjects)
                {
                    Button button = getSubjectBtn(subject);
                    buttons.add(button);
                }

                subjectListView.setItems(buttons);
            });
        });
    }

    private Button getSubjectBtn(Subject subject)
    {
        String name = subject.getName();

        Button button = new Button(name);
        button.setMinWidth(150);
        button.setMaxWidth(200);
        button.setCenterShape(true);
        button.setStyle(
                "-fx-background-color: #212121;" +
                        "-fx-border-color: white;" +
                        "-fx-border-width: 1px;" +
                        "-fx-text-fill: white;"
        );
        button.setAlignment(Pos.CENTER);
        button.setOnAction(event -> handleButtonAction(name));
        return button;
    }

    private void handleButtonAction(String name)
    {
        Loader.getInstance().runProcessWithLoading(rootPane, () ->
        {
            ObservableList<Class> classes = FXCollections.observableArrayList();

            // Get classes from subject
            //
            subjects.forEach(subject ->
            {
                if (!subject.getName().equals(name))
                    return;

                classes.addAll(subject.getClasses());
                ClassListController.setSubject(subject);
            });

            // Set classes to ClassListController
            //
            ClassListController.setClasses(classes);

            // Load ClassListController
            //
            Platform.runLater(() ->
            {
                try
                {
                    Parent node = FXMLLoader.load(Objects.requireNonNull(App.class.getResource("class-list-view.fxml")));
                    rootPane.getScene().setRoot(node);
                }
                catch (IOException e)
                {
                    throw new RuntimeException(e);
                }
            });
        });
    }

    public void onBackButtonClick(ActionEvent actionEvent)
    {
        try
        {
            Parent node = FXMLLoader.load(Objects.requireNonNull(App.class.getResource("registration-list-view.fxml")));
            rootPane.getScene().setRoot(node);
        }
        catch (IOException e)
        {
            throw new RuntimeException(e);
        }
    }

    public void SearchPreferences(ActionEvent actionEvent)
    {
        try
        {
            Parent node = FXMLLoader.load(Objects.requireNonNull(App.class.getResource("search-preferences-view.fxml")));
            rootPane.getScene().setRoot(node);
        }
        catch (IOException e)
        {
            throw new RuntimeException(e);
        }
    }

}
