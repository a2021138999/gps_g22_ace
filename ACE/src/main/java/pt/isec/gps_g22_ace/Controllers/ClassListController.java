package pt.isec.gps_g22_ace.Controllers;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.io.IOException;

import javafx.fxml.FXML;
import javafx.geometry.Pos;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.stage.Modality;
import javafx.stage.Screen;
import javafx.scene.text.Font;
import javafx.fxml.FXMLLoader;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.scene.paint.Color;
import javafx.event.ActionEvent;
import javafx.scene.text.FontPosture;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;

import javafx.stage.Stage;
import javafx.stage.StageStyle;
import pt.isec.gps_g22_ace.App;
import pt.isec.gps_g22_ace.ace.data.Class;
import pt.isec.gps_g22_ace.ace.data.Subject;

public class ClassListController
{
    @FXML
    public VBox rootPane;

    @FXML
    private ListView<HBox> classListView;
    @FXML
    public Label title;
    private static ObservableList<Class> classes;

    private static Subject subject;

    public static void setClasses(ObservableList<Class> classes)
    {
        ClassListController.classes = classes;
    }

    public static void setSubject(Subject subject) {
        ClassListController.subject = subject;
    }

    public void initialize()
    {
        title.setText("Classes List");
        title.setTextFill(Color.WHITE);
        title.setAlignment(Pos.TOP_CENTER);

        ObservableList<HBox> cardList = FXCollections.observableArrayList();
        for (Class class_ : classes)
        {
            HBox card = createCard(class_);
            cardList.add(card);
        }

        classListView.setItems(cardList);
    }

    private HBox createCard(Class c)
    {
        HBox card = new HBox(20); // Espaçamento entre os elementos do card
        card.getStyleClass().add("card"); // Aplicar o estilo CSS aos cartões
        card.setStyle(
                "-fx-background-color:  #2f2d2d;" + // Cor de fundo branca
                        "-fx-text-fill: white;" + // Cor do texto
                        "-fx-border-color: #212121;" + // Cor da borda
                        "-fx-border-width: 2px;" +
                        "-fx-border-radius: 0px;" + // Borda quadrada
                        "-fx-padding: 10px;" // Espaçamento interno
        );

        if(c.isEnrolled())
            card.setStyle("-fx-background-color: #318017");

        Label nameLabel = new Label("Class: " + c.getName() + "\n");
        Label teacherLabel = new Label("Teacher: " + c.getTeacherName() + "\n");
        Label vacancies = new Label("Vacancies Left: "+c.getVacancies());
        Button detailsButton = new Button("Details");
        detailsButton.setStyle("-fx-background-color: #212121; -fx-text-fill: white; -fx-font-size: 10px; -fx-padding: 4px 8px;");

        nameLabel.getStyleClass().add("label");
        teacherLabel.getStyleClass().add("label");
        vacancies.getStyleClass().add("label");

        if(c.getVacancies() == 0)
            vacancies.setStyle("-fx-text-fill: red");

        double fontSize = 0.02 * Screen.getPrimary().getVisualBounds().getWidth();
        Font proportionalFont = Font.font("Arial", FontPosture.REGULAR, fontSize);

        nameLabel.setFont(proportionalFont);
        teacherLabel.setFont(proportionalFont);

        card.getChildren().addAll(new VBox(nameLabel, teacherLabel,  vacancies, detailsButton));

        detailsButton.setOnMouseClicked(mouseEvent ->  {
            System.out.println(subject.getName());
            VBox popupContent = new VBox(10);
            popupContent.setAlignment(Pos.CENTER);
            popupContent.setStyle("-fx-background-color: #212121; fx-text-fill: white; -fx-padding: 20px;");
            VBox info = new VBox(
                    new Label("Type:" + c.getType()),
                    new Label("Class:" + c.getName()),
                    new Label("Lessons:" + c.getNumLessons()),
                    new Label("Teacher:" + c.getTeacherName())
            );

            for (Node label : info.getChildren()) {
                if (label instanceof Label) {
                    ((Label) label).setTextFill(Color.WHITE);
                }
            }
            Button exitButton = new Button("Exit");
            exitButton.setStyle("-fx-background-color: #9a0303; -fx-text-fill: white;"); // Green button

            popupContent.getChildren().addAll(info, exitButton);
            popupContent.setAlignment(Pos.CENTER);
            popupContent.setSpacing(20);
            Stage popupStage = new Stage();
            popupStage.initModality(Modality.APPLICATION_MODAL);
            popupStage.initOwner(rootPane.getScene().getWindow());
            popupStage.setScene(new Scene(popupContent, 200, 150));
            popupStage.initStyle(StageStyle.UNDECORATED);


            exitButton.setOnAction(closeEvent -> {
                popupStage.close();
            });

            popupStage.showAndWait();
        });

        if (c.canEnroll() && !c.isEnrolled())
            card.setOnMouseClicked(event -> {
                VBox popupContent = new VBox(10);
                popupContent.setAlignment(Pos.CENTER);
                popupContent.setStyle("-fx-background-color: #212121; -fx-padding: 20px;");

                Label className = new Label("Class:" + c.getName());
                className.setTextFill(Color.WHITE); // Set text color to white

                Button enrollButton = new Button("Enroll");
                Button exitButton = new Button("Exit");
                enrollButton.setStyle("-fx-background-color: #4CAF50; -fx-text-fill: white;"); // Green button
                exitButton.setStyle("-fx-background-color: #9a0303; -fx-text-fill: white;");
                HBox btns = new HBox(enrollButton, exitButton);
                btns.setAlignment(Pos.CENTER);
                btns.setSpacing(20);

                popupContent.getChildren().addAll(className, btns);
                popupContent.setAlignment(Pos.CENTER);
                popupContent.setSpacing(20);
                Stage popupStage = new Stage();
                popupStage.initModality(Modality.APPLICATION_MODAL);
                popupStage.initOwner(rootPane.getScene().getWindow());
                popupStage.setScene(new Scene(popupContent, 200, 150));
                popupStage.initStyle(StageStyle.UNDECORATED);

                enrollButton.setOnAction(enrollEvent ->
                {
                    card.setStyle("-fx-background-color: #ffb537");
                    if (!subject.enroll(c))
                    {
                        // Create the popup content
                        VBox popupContent1 = new VBox(10);
                        popupContent1.setAlignment(Pos.CENTER);
                        popupContent1.setStyle("-fx-background-color: #9a0303; -fx-border-width: 2px; -fx-border-color: white; -fx-padding: 20px; -fx-text-fill: white;");

                        Label className1 = new Label("Failed to enroll in " + c.getName());
                        className1.setTextFill(Color.WHITE); // Set text color to white
                        Button exitButton1 = new Button("Back");
                        exitButton1.setStyle("-fx-background-color: #212121; -fx-text-fill: white;");
                        HBox btns1 = new HBox(exitButton1);
                        btns1.setAlignment(Pos.CENTER);
                        btns1.setSpacing(20);

                        popupContent1.getChildren().addAll(className1, btns1);
                        popupContent1.setAlignment(Pos.CENTER);
                        popupContent1.setSpacing(20);

                        // Criar a cena e a janela do popup
                        Scene popupScene1 = new Scene(popupContent1, 200, 100);
                        Stage popupStage1 = new Stage();
                        popupStage1.initModality(Modality.APPLICATION_MODAL);
                        popupStage1.setScene(popupScene1);
                        popupStage1.initStyle(StageStyle.UNDECORATED);

                        // Exibir o popup
                        popupStage1.show();

                        exitButton1.setOnAction(closeEvent -> {
                            popupStage1.close();
                        });
                    }

                    popupStage.close();
                    });

                exitButton.setOnAction(closeEvent -> {
                    popupStage.close();
                });
                popupStage.showAndWait();
            });

        return card;
    }

    public void onBackButtonClick(ActionEvent actionEvent)
    {
        try
        {
            Parent node = FXMLLoader.load(Objects.requireNonNull(App.class.getResource("subject-list-view.fxml")));
            rootPane.getScene().setRoot(node);
        }
        catch (IOException e)
        {
            throw new RuntimeException(e);
        }
    }

    public void SearchPreferences(ActionEvent actionEvent) {
        try
        {
            Parent node = FXMLLoader.load(Objects.requireNonNull(App.class.getResource("search-preferences-view.fxml")));
            rootPane.getScene().setRoot(node);
        }
        catch (IOException e)
        {
            throw new RuntimeException(e);
        }
    }


    public void onSaveChanges(ActionEvent actionEvent) {
        boolean processResult = subject.processEnroll();

        if (processResult) {

            System.out.println("Enrolled  successfully.");
            // Create the popup content
            VBox popupContent1 = new VBox(10);
            popupContent1.setAlignment(Pos.CENTER);
            popupContent1.setStyle("-fx-background-color: #318017; " +
                    "-fx-padding: 20px; " +
                    "-fx-border-color: white;" +
                    " -fx-border-width: 2px; ");

            Label className1 = new Label("Enrolled in successfully");
            className1.setTextFill(Color.WHITE); // Set text color to white
            Button exitButton1 = new Button("Back");
            exitButton1.setStyle("-fx-background-color: #212121; -fx-text-fill: white;");
            HBox btns1 = new HBox(exitButton1);
            btns1.setAlignment(Pos.CENTER);
            btns1.setSpacing(20);

            popupContent1.getChildren().addAll(className1, btns1);
            popupContent1.setAlignment(Pos.CENTER);
            popupContent1.setSpacing(20);

            // Criar a cena e a janela do popup
            Scene popupScene1 = new Scene(popupContent1, 200, 100);
            Stage popupStage1 = new Stage();
            popupStage1.initModality(Modality.APPLICATION_MODAL);
            popupStage1.setScene(popupScene1);
            popupStage1.initStyle(StageStyle.UNDECORATED);

            // Exibir o popup
            popupStage1.show();

            exitButton1.setOnAction(closeEvent -> {
                popupStage1.close();
            });
        } else {
            System.out.println("Failed to enroll in ");
            // Create the popup content
            VBox popupContent1 = new VBox(10);
            popupContent1.setAlignment(Pos.CENTER);
            popupContent1.setStyle("-fx-background-color: #9a0303; -fx-border-width: 2px; -fx-border-color: white; -fx-padding: 20px; -fx-text-fill: white;");

            Label className1 = new Label("Failed to enroll ");
            className1.setTextFill(Color.WHITE); // Set text color to white
            Button exitButton1 = new Button("Back");
            exitButton1.setStyle("-fx-background-color: #212121; -fx-text-fill: white;");
            HBox btns1 = new HBox(exitButton1);
            btns1.setAlignment(Pos.CENTER);
            btns1.setSpacing(20);

            popupContent1.getChildren().addAll(className1, btns1);
            popupContent1.setAlignment(Pos.CENTER);
            popupContent1.setSpacing(20);

            // Criar a cena e a janela do popup
            Scene popupScene1 = new Scene(popupContent1, 200, 100);
            Stage popupStage1 = new Stage();
            popupStage1.initModality(Modality.APPLICATION_MODAL);
            popupStage1.setScene(popupScene1);
            popupStage1.initStyle(StageStyle.UNDECORATED);

            // Exibir o popup
            popupStage1.show();

            exitButton1.setOnAction(closeEvent -> {
                popupStage1.close();
            });
        }
        initialize();
    }
}

