package pt.isec.gps_g22_ace.Controllers;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.collections.ObservableMap;
import javafx.event.ActionEvent;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.control.Label;
import javafx.scene.control.ListView;
import javafx.scene.control.TextField;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.scene.text.Font;
import javafx.scene.text.FontPosture;
import javafx.stage.Screen;
import pt.isec.gps_g22_ace.App;
import pt.isec.gps_g22_ace.ace.data.Class;
import pt.isec.gps_g22_ace.ace.data.Subject;

import java.io.IOException;
import java.util.List;
import java.util.Objects;

public class EffortsController {
    public ListView<HBox> resultList;
    public VBox rootPane;
    private static List<String> resMap;

    public static void setResults(List<String> results)
    {
        EffortsController.resMap = results;
    }

    public void initialize()
    {
        ObservableList<HBox> observableList = FXCollections.observableArrayList();
        for(String s : resMap){
            Label l = new Label(s);
            if(s.contains("Atualmente")){
                l.setStyle("-fx-text-fill: #e3da30");
            }else{
                l.setStyle("-fx-text-fill: green");
            }
            observableList.add(new HBox(l));
        }
        resultList.setCenterShape(true);
        resultList.setStyle("-fx-text-alignment: center");
        resultList.setItems(observableList);
    }



    public void onBackButtonClick(ActionEvent actionEvent)
    {
        try
        {
            Parent node = FXMLLoader.load(Objects.requireNonNull(App.class.getResource("subject-list-view.fxml")));
            rootPane.getScene().setRoot(node);
        }
        catch (IOException e)
        {
            throw new RuntimeException(e);
        }
    }
}
