package pt.isec.gps_g22_ace.Controllers;

import java.util.List;
import java.util.Objects;
import java.io.IOException;

import javafx.fxml.FXML;
import javafx.geometry.Pos;
import javafx.fxml.FXMLLoader;
import javafx.event.ActionEvent;
import javafx.scene.Parent;
import javafx.scene.layout.StackPane;
import javafx.scene.layout.VBox;
import javafx.scene.paint.Color;
import javafx.scene.control.Label;
import javafx.scene.control.Button;
import javafx.application.Platform;
import javafx.scene.control.ListView;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;

import pt.isec.gps_g22_ace.App;
import pt.isec.gps_g22_ace.ace.ACE;
import pt.isec.gps_g22_ace.ace.SearchAlgorithm;
import pt.isec.gps_g22_ace.utils.Loader;
import pt.isec.gps_g22_ace.ace.data.User;
import pt.isec.gps_g22_ace.ace.data.Subject;
import pt.isec.gps_g22_ace.ace.data.Registration;

public class RegistrationListController
{
    @FXML
    public VBox rootPane;

    @FXML
    public ListView<Button> registrationListView;
    @FXML
    public Label title;

    private final ACE ace = ACE.getInstance();
    private User user = null;
    private List<Registration> registrations = null;

    public void initialize()
    {
        Loader.getInstance().runProcessWithLoading(rootPane, () ->
        {
            user = ace.getUser();

            Platform.runLater(() ->
            {
                title.setText("Welcome, " + user.getName() + "!");
                title.setTextFill(Color.WHITE);
                title.setAlignment(Pos.TOP_CENTER);

                registrations = user.getRegistrations();
                if (registrations == null || registrations.isEmpty())
                    return;

                ObservableList<Button> buttons = FXCollections.observableArrayList();
                for (Registration registration: registrations)
                {
                    Button button = getRegistrationBtn(registration);
                    buttons.add(button);
                }

                registrationListView.setItems(buttons);
            });
        });
    }

    private Button getRegistrationBtn(Registration registration)
    {
        String name = registration.getName();

        Button button = new Button(name);
        button.setMinWidth(150);
        button.setMaxWidth(200);
        button.setCenterShape(true);
        button.setStyle(
                "-fx-background-color: #212121;" +
                        "-fx-border-color: white;" +
                        "-fx-border-width: 1px;" +
                        "-fx-text-fill: white;"
        );
        button.setAlignment(Pos.CENTER);
        button.setOnAction(event -> handleButtonAction(name));
        return button;
    }

    private void handleButtonAction(String name)
    {
        Loader.getInstance().runProcessWithLoading(rootPane, () ->
        {
            ObservableList<Subject> subjects = FXCollections.observableArrayList();

            // Get subjects from registration
            //
            for (Registration registration : registrations)
            {
                if (!registration.getName().equals(name))
                    continue;

                SearchAlgorithm.setRegistration(registration);
                subjects.addAll(registration.getSubjects());
            }

            // Set subjects to SubjectListController
            //
            SubjectListController.setSubjects(subjects);

            // Load SubjectListController
            //
            Platform.runLater(() ->
            {
                try
                {
                    Parent node = FXMLLoader.load(Objects.requireNonNull(App.class.getResource("subject-list-view.fxml")));
                    rootPane.getScene().setRoot(node);
                }
                catch (IOException e)
                {
                    throw new RuntimeException(e);
                }
            });
        });
    }

    public void accountLogout(ActionEvent actionEvent)
    {
        Loader.getInstance().runProcessWithLoading(rootPane, () ->
        {
            // TODO: Show error message if logout fails
            //
            if (!ace.logout())
            {
                System.out.println("Failed to logout: " + ace.getLastError());
                return;
            }

            // Load LoginController
            //
            Platform.runLater(() ->
            {
                try
                {
                    Parent node = FXMLLoader.load(Objects.requireNonNull(App.class.getResource("login-view.fxml")));
                    rootPane.getScene().setRoot(node);
                }
                catch (IOException e)
                {
                    throw new RuntimeException(e);
                }
            });
        });
    }
}
