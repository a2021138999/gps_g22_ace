package pt.isec.gps_g22_ace.utils;

import javafx.application.Platform;
import javafx.geometry.Pos;
import javafx.scene.Node;
import javafx.scene.Scene;
import javafx.scene.control.Label;
import javafx.scene.control.ProgressIndicator;
import javafx.scene.layout.BorderPane;
import javafx.geometry.Insets;
import javafx.scene.layout.StackPane;
import javafx.stage.Modality;
import javafx.stage.Stage;
import javafx.stage.StageStyle;

public class Loader
{
    private static Loader instance;
    private static final double MAX_SIZE = 150;

    private final Scene loaderScene;

    private Loader()
    {
        ProgressIndicator progressIndicator = new ProgressIndicator();

        Label label = new Label("Loading...");
        label.setStyle("-fx-text-fill: white;");
        label.setAlignment(Pos.CENTER);

        BorderPane loaderPane = new BorderPane();
        loaderPane.setCenter(progressIndicator);

        // Set bottom padding for the label
        BorderPane.setMargin(label, new Insets(0, 0, 10, 0)); // 10 pixels bottom padding
        BorderPane.setAlignment(label, Pos.BOTTOM_CENTER);

        loaderPane.setBottom(label);
        loaderPane.setMaxSize(MAX_SIZE, MAX_SIZE);
        loaderPane.setStyle("-fx-background-color: rgba(0, 0, 0, 0.5);");

        loaderScene = new Scene(loaderPane, MAX_SIZE * 2, MAX_SIZE);
        loaderScene.setFill(null);
    }


    public static Loader getInstance()
    {
        if (instance == null)
            instance = new Loader();

        return instance;
    }

    public void startLoading(Node parentNode)
    {
        Platform.runLater(() ->
        {
            Scene scene = parentNode.getScene();

            Stage stage = new Stage(StageStyle.TRANSPARENT);
            stage.initOwner(scene.getWindow());
            stage.initModality(Modality.APPLICATION_MODAL);

            stage.setScene(loaderScene);

            stage.setX(scene.getWindow().getX() + (scene.getWidth() - MAX_SIZE * 2) / 2);
            stage.setY(scene.getWindow().getY() + (scene.getHeight() - MAX_SIZE) / 2);

            stage.show();
        });
    }

    public void stopLoading(Node parentNode)
    {
        Platform.runLater(() ->
        {
            ((Stage)loaderScene.getWindow()).close();
        });
    }

    public void runProcessWithLoading(Node parentNode, Runnable process)
    {
        new Thread(() ->
        {
            try
            {
                startLoading(parentNode);
                process.run();
            }
            finally
            {
                stopLoading(parentNode);
            }
        }).start();
    }
}