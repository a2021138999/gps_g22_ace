package pt.isec.gps_g22_ace.utils;

import javax.print.DocFlavor;
import java.io.*;

public class RecordingManager {
    public static RecordingManager _instance=null;
    private RecordingManager(){

    }
    public static RecordingManager getInstance(){
        if(_instance==null){
            _instance=new RecordingManager();
        }
        return _instance;
    }
    public Object loadFromFile(String fileName){
        Object object = null;
        try(ObjectInputStream in = new ObjectInputStream((new FileInputStream(fileName)))) {
            object= in.readObject();
        } catch (IOException | ClassNotFoundException e) {
            e.printStackTrace();
        }
        return object;
    }
    public boolean writeToFile(String fileName, Object object){
        try(ObjectOutputStream out=new ObjectOutputStream(new FileOutputStream(fileName))) {
            out.writeObject(object);
            out.flush();
        } catch (IOException e) {
            return false;
        }
        return true;
    }
}
