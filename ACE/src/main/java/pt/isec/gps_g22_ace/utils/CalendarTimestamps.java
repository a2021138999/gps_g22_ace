package pt.isec.gps_g22_ace.utils;

import java.time.*;
import java.time.format.DateTimeFormatter;

public class CalendarTimestamps
{
    private static final ZoneId LOCAL_ZONE = ZoneId.systemDefault();
    private final static DateTimeFormatter dateTimeFormatter = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm").withZone(LOCAL_ZONE);

    public static ZonedDateTime getCurrentDateTime()
    {
        return ZonedDateTime.now(LOCAL_ZONE);
    }

    public static long getCurrentTimestamp()
    {
        return getCurrentDateTime().toEpochSecond();
    }

    public static ZonedDateTime getZonedDateTimeFromStr(String str)
    {
        return ZonedDateTime.parse(str, dateTimeFormatter);
    }

    public static long getTimestampFromLocalDate(LocalDate date)
    {
        return date.atStartOfDay(LOCAL_ZONE).toEpochSecond();
    }
}
