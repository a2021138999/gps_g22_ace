package pt.isec.gps_g22_ace.ace.data;

import pt.isec.gps_g22_ace.ace.Exceptions;
import pt.isec.gps_g22_ace.ace.NonioScraper;

import java.util.ArrayList;
import java.util.List;

public class User
{
    private final NonioScraper scraper;

    public User(NonioScraper scraper)
    {
        this.scraper = scraper;
    }

    public String getName()
    {
        try
        {
            return scraper.getName();
        }
        catch (Exceptions.UserNotLoggedIn e)
        {
            System.out.println("User not logged in");
            return null;
        } catch (Exceptions.InvalidStatus e)
        {
            System.out.println("Invalid status: " + e.getMessage());
            return null;
        } catch (Exceptions.Warning e)
        {
            System.out.println("Warning: " + e.getMessage());
            return null;
        }
    }

    public List<Registration> getRegistrations()
    {
        try
        {
            return scraper.getRegistrations();
        }
        catch (Exceptions.UserNotLoggedIn e)
        {
            System.out.println("User not logged in");
            return null;
        }
        catch (Exceptions.InvalidStatus e)
        {
            System.out.println("Invalid status: " + e.getMessage());
            return null;
        }
        catch (Exceptions.Warning e)
        {
            System.out.println("Warning: " + e.getMessage());
            return null;
        }
    }
}