package pt.isec.gps_g22_ace.ace;

import pt.isec.gps_g22_ace.ace.data.*;
import pt.isec.gps_g22_ace.ace.data.Class;

import java.util.*;
import java.util.stream.Collectors;

public class SearchAlgorithm
{
    private static final int MAX_SEARCH_DEPTH = 10;
    private final Map<Subject, List<Schedule>> blacklistedSchedules = new HashMap<>();

    private static Registration registration;

    public static void setRegistration(Registration registration)
    {
        SearchAlgorithm.registration = registration;
    }

    public Map<Subject, List<Class>> search(Preferences preferences) throws Exceptions.NoClassesAvailable
    {
        List<Subject> subjectList = registration.getSubjects();
        Map<Subject, List<Class>> classList = subjectList.stream()
                .collect(Collectors.toMap(s -> s, Subject::getClasses));

        for (int i = 0; i < MAX_SEARCH_DEPTH; i++)
        {
            blacklistedSchedules.clear();

            try
            {
                return search(classList, preferences);
            }
            catch (Exceptions.NoClassesAvailable ignored) { }
        }

        throw new Exceptions.NoClassesAvailable();
    }

    private Map<Subject, List<Class>> search(Map<Subject, List<Class>> classList, Preferences preferences) throws Exceptions.NoClassesAvailable
    {
        for (Subject subject : classList.keySet())
        {
            // Skip subjects with no classes
            //
            if (classList.get(subject).isEmpty())
                continue;

            List<Class> classes = searchSubject(subject.getAvailableTypes(), classList.get(subject), preferences);

            // Add new schedules to blacklist
            //
            for (Class c : classes)
            {
                if (blacklistedSchedules.containsKey(subject))
                    blacklistedSchedules.get(subject).add(c.getSchedule());
                else
                    blacklistedSchedules.put(subject, new ArrayList<>(Collections.singletonList(c.getSchedule())));
            }

            classList.put(subject, classes);
        }

        return classList;
    }

    public List<Class> searchSubject(Subject subject, Preferences preferences) throws Exceptions.NoClassesAvailable
    {
        List<Class> classList = searchSubject(subject.getAvailableTypes(), subject.getClasses(), preferences);

        // Add new schedules to blacklist
        //
        for (Class c : classList)
        {
            if (blacklistedSchedules.containsKey(subject))
                blacklistedSchedules.get(subject).add(c.getSchedule());
            else
                blacklistedSchedules.put(subject, new ArrayList<>(Collections.singletonList(c.getSchedule())));
        }

        return classList;
    }

    private List<Class> searchSubject(Set<Class.Type> availableTypes, List<Class> subjectClassList, Preferences preferences) throws Exceptions.NoClassesAvailable
    {
        // Filter out classes that are not available
        //
        List<Class> availableClasses = subjectClassList.stream()
                .filter(c -> (c.canEnroll() || c.isEnrolled()))
                .toList();

        if (availableClasses.isEmpty())
            throw new Exceptions.NoClassesAvailable();

        // Filter out classes that are blacklisted from all subjects
        //
        availableClasses = availableClasses.stream()
                .filter(c -> blacklistedSchedules.values().stream().noneMatch(l -> l.contains(c.getSchedule())))
                .toList();

        if (availableClasses.isEmpty())
           throw new Exceptions.NoClassesAvailable();

        // Filter out classes that do not match the given filters
        //
        List<Filter> filters = preferences.getFilterList();
        for (Filter filter : filters)
            availableClasses = availableClasses.stream().filter(filter::apply).toList();

        // Check if there is at least one class of all available types
        //


        for (Class.Type type : availableTypes)
        {
            if (availableClasses.stream().anyMatch(c -> c.getType() == type))
                continue;

           throw new Exceptions.NoClassesAvailable();
        }


        // Get a random class of each type
        //
        return availableClasses.stream()
                .filter(c -> availableTypes.contains(c.getType()))
                .collect(Collectors.groupingBy(Class::getType))
                .values()
                .stream()
                .map(l -> l.get(new Random().nextInt(l.size())))
                .toList();
    }
}
