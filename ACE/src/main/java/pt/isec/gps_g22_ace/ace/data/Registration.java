package pt.isec.gps_g22_ace.ace.data;

import pt.isec.gps_g22_ace.ace.Exceptions;
import pt.isec.gps_g22_ace.ace.NonioScraper;

import java.util.List;

public class Registration
{
    private final long id;
    private final String name;
    private final NonioScraper scraper;

    public Registration(NonioScraper scraper, long id, String name)
    {
        this.id = id;
        this.name = name;
        this.scraper = scraper;
    }

    public long getId()
    {
        return id;
    }

    public String getName()
    {
        return name;
    }

    public List<Subject> getSubjects()
    {
        try
        {
            return scraper.getSubjects(id);
        }
        catch (Exceptions.UserNotLoggedIn e)
        {
            System.out.println("User not logged in");
            return null;
        }
        catch (Exceptions.InvalidStatus e)
        {
            System.out.println("Invalid status: " + e.getMessage());
            return null;
        }
        catch (Exceptions.Warning e)
        {
            System.out.println("Warning: " + e.getMessage());
            return null;
        }
    }
}