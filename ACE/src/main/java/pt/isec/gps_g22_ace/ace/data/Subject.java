package pt.isec.gps_g22_ace.ace.data;

import pt.isec.gps_g22_ace.ace.Exceptions;
import pt.isec.gps_g22_ace.ace.NonioScraper;

import java.time.ZonedDateTime;
import java.util.*;
import java.util.stream.Collectors;

public class Subject
{
    private Map<Long, Schedule> schedules = null;
    private final long id, code;
    private final int semester;
    private final String name;
    private final ZonedDateTime startDate, endDate;
    private final boolean canEnroll;
    private final NonioScraper scraper;
    private final Map<Class.Type, Class> enrolledClasses = new HashMap<>();

    public Subject(NonioScraper scraper, long id, long code, String name, int semester, ZonedDateTime start, ZonedDateTime endDate, boolean canEnroll)
    {
        this.scraper = scraper;
        this.id = id;
        this.code = code;
        this.name = name;
        this.semester = semester;
        this.startDate = start;
        this.endDate = endDate;
        this.canEnroll = canEnroll;

        updateSchedule();
    }

    private void updateSchedule()
    {
        try
        {
            schedules = scraper.getSchedule(id);
        }
        catch (Exceptions.UserNotLoggedIn e)
        {
            System.out.println("User not logged in");
        }
        catch (Exceptions.InvalidStatus e)
        {
            System.out.println("Invalid status: " + e.getMessage());
        }
        catch (Exceptions.Warning e)
        {
            System.out.println("Warning: " + e.getMessage());
        }
    }

    public long getId()
    {
        return id;
    }

    public String getName()
    {
        return name;
    }

    public long getCode()
    {
        return code;
    }

    public int getSemester()
    {
        return semester;
    }

    public ZonedDateTime getStartDate()
    {
        return startDate;
    }

    public ZonedDateTime getEndDate()
    {
        return endDate;
    }

    public boolean canEnroll()
    {
        return canEnroll;
    }

    public List<Class> getClasses()
    {
        if (!canEnroll)
            return new ArrayList<>();

        try
        {
            List<Class> classes = scraper.getClasses(id);

            for (Class c: classes)
            {
                if (c.isEnrolled())
                    this.enrolledClasses.put(c.getType(), c);

                if (schedules == null)
                {
                    System.out.println("Schedules not loaded");
                    continue;
                }

                Schedule schedule = schedules.getOrDefault(c.getId(), null);
                if (schedule == null)
                {
                    System.out.println("Schedule for class " + c.getId() + " not found");
                    continue;
                }

                c.setSchedule(schedule);
            }

            return classes;
        }
        catch (Exceptions.UserNotLoggedIn e)
        {
            System.out.println("User not logged in");
            return null;
        }
        catch (Exceptions.InvalidStatus e)
        {
            System.out.println("Invalid status: " + e.getMessage());
            return null;
        }
        catch (Exceptions.InvalidClassType e)
        {
            System.out.println("Invalid class type: " + e.getMessage());
            return null;
        } catch (Exceptions.Warning e)
        {
            System.out.println("Warning: " + e.getMessage());
            return null;
        }
    }

    private boolean needsSubmit = false;

    public boolean processEnroll()
    {
        if (!canEnroll)
            return false;

        if (!needsSubmit)
            return true;

        Class theoretical = enrolledClasses.getOrDefault(Class.Type.THEORETICAL, null);
        Class practical = enrolledClasses.getOrDefault(Class.Type.PRACTICAL, null);
        Class theoreticPractical = enrolledClasses.getOrDefault(Class.Type.THEORETIC_PRACTICAL, null);
        try
        {
            scraper.enroll(id, theoretical != null ? theoretical.getId() : 0, practical != null ? practical.getId() : 0, theoreticPractical != null ? theoreticPractical.getId() : 0);
        }
        catch (Exceptions.UserNotLoggedIn e)
        {
            System.out.println("User not logged in");
            return false;
        }
        catch (Exceptions.InvalidStatus e)
        {
            System.out.println("Invalid status: " + e.getMessage());
            return false;
        }
        catch (Exceptions.Warning e)
        {
            System.out.println("Warning: " + e.getMessage());
            return false;
        }

        return true;
    }

    public boolean enroll(Class c)
    {
        if (c == null || c.isEnrolled() || !c.canEnroll())
            return false;

        Class old = enrolledClasses.put(c.getType(), c);
        if (old != null)
            old.setEnrolled(false);
        c.setEnrolled(true);

        needsSubmit = true;
        return true;
    }

    public Set<Class.Type> getAvailableTypes()
    {
        return enrolledClasses.keySet();
    }
}