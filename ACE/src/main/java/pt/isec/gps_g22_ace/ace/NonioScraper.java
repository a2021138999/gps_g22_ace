package pt.isec.gps_g22_ace.ace;

import java.awt.image.BufferedImage;
import java.io.IOException;
import java.net.URL;
import java.net.URLConnection;
import java.time.DayOfWeek;
import java.time.LocalDate;
import java.time.ZoneId;
import java.time.ZonedDateTime;
import java.time.format.DateTimeFormatter;
import java.time.temporal.TemporalAdjusters;
import java.util.*;
import javax.imageio.ImageIO;

import org.json.JSONArray;
import org.json.JSONObject;

import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;

import org.jsoup.select.Elements;
import pt.isec.gps_g22_ace.ace.data.Class;
import pt.isec.gps_g22_ace.ace.data.Schedule;
import pt.isec.gps_g22_ace.ace.data.Subject;
import pt.isec.gps_g22_ace.ace.data.Registration;
import pt.isec.gps_g22_ace.utils.CalendarTimestamps;

import static pt.isec.gps_g22_ace.utils.CalendarTimestamps.getCurrentDateTime;

public class NonioScraper
{
    private final String domain;
    private boolean isLogged = false, needsCaptcha = false;
    private final Session session = new Session();
    private final static DateTimeFormatter dateTimeFormatter = DateTimeFormatter.ofPattern("dd-MM-yyyy HH:mm");

    public NonioScraper(String domain)
    {
        this.domain = domain;
    }

    private Map<String, String> getLoginData(String username, String password, String captchaAnswer)
    {
        Map<String, String> data = new HashMap<>();
        data.put("tipoCaptcha", "text");
        data.put("username", username);
        data.put("password", password);

        if (captchaAnswer != null)
            data.put("captcha", captchaAnswer);

        return data;
    }

    public boolean needsCaptcha()
    {
        return needsCaptcha;
    }

    public boolean isLogged()
    {
        return isLogged;
    }

    public void login(String username, String password, String captchaAnswer) throws Exceptions.UserAlreadyLoggedIn, Exceptions.Warning, Exceptions.InvalidStatus
    {
        if (isLogged)
            throw new Exceptions.UserAlreadyLoggedIn();

        try
        {
            // Using the login page to get the cookies
            //
            Document loginPage = session.get(domain + "/nonio/security/login.do");
            int loginPageStatusCode = loginPage.connection().response().statusCode();
            if (loginPageStatusCode != 200)
                throw new Exceptions.InvalidStatus(loginPageStatusCode);

            // Check if we are already logged in (99% not)
            //
            if (loginPage.location().contains("/nonio/dashboard/dashboard.do"))
            {
                isLogged = true;
                needsCaptcha = false;
                return;
            }

            // Send the login request
            //
            Document loginResponse = session.post(domain + "/nonio/security/login.do?method=submeter", getLoginData(username, password, captchaAnswer));
            int loginResponseStatusCode = loginResponse.connection().response().statusCode();
            if (loginResponseStatusCode != 200)
                throw new Exceptions.InvalidStatus(loginResponseStatusCode);

            // Check if we are logged in
            //
            if (loginResponse.location().contains("/nonio/dashboard/dashboard.do"))
            {
                isLogged = true;
                needsCaptcha = false;
                return;
            }

            isLogged = false;

            String warningText = "";
            Element warningElement = loginResponse.getElementById("div_erros_preenchimento_formulario");
            if (warningElement != null)
                warningText = warningElement.text();

            Element captchaTable = loginResponse.getElementsByClass("captchaTable").first();
            needsCaptcha = (captchaTable != null);

            throw new Exceptions.Warning(warningText);
        }
        catch (IOException e)
        {
            throw new Exceptions.Warning(e.getMessage());
        }
    }

    public void logout() throws Exceptions.UserNotLoggedIn, Exceptions.InvalidStatus, Exceptions.Warning
    {
        if (!isLogged)
            throw new Exceptions.UserNotLoggedIn();

        try
        {
            Document logoutResponse = session.get(domain + "/nonio/security/logout.do", false);

            int statusCode = logoutResponse.connection().response().statusCode();
            if (statusCode != 302)
                throw new Exceptions.InvalidStatus(statusCode);
        }
        catch (IOException e)
        {
            throw new Exceptions.Warning(e.getMessage());
        }

        isLogged = false;
        session.clearAll();
    }

    public BufferedImage getCaptchaImage() throws Exceptions.Warning
    {
        try
        {
            URLConnection connection = new URL(domain + "/nonio/simpleCaptchaImg").openConnection();

            for (Map.Entry<String, String> cookie : session.getCookies().entrySet())
                connection.setRequestProperty("Cookie",
                        cookie.getKey() + "=" + cookie.getValue());

            for (Map.Entry<String, String> header : session.getHeaders().entrySet())
                connection.setRequestProperty("Header",
                        header.getKey() + "=" + header.getValue());

            connection.connect();

            return ImageIO.read(connection.getInputStream());
        }
        catch (IOException e)
        {
            throw new Exceptions.Warning(e.getMessage());
        }
    }

    public String getName() throws Exceptions.UserNotLoggedIn, Exceptions.InvalidStatus, Exceptions.Warning
    {
        if (!isLogged)
            throw new Exceptions.UserNotLoggedIn();

        try
        {
            // Using the dashboard page to get the name
            //
            Document dashboardPage = session.get(domain + "/nonio/dashboard/dashboard.do");
            int dashboardPageStatusCode = dashboardPage.connection().response().statusCode();
            if (dashboardPageStatusCode != 200)
                throw new Exceptions.InvalidStatus(dashboardPageStatusCode);

            // Check if we are really logged in
            //
            if (!dashboardPage.location().contains("/nonio/dashboard/dashboard.do"))
                throw new Exceptions.UserNotLoggedIn();

            // Get the name
            //
            String fullName = Objects.requireNonNull(dashboardPage.select("span[class='hidden-tablet']").first()).text();

            int fullNameIndex = fullName.indexOf(" |");
            if (fullNameIndex != -1)
                fullName = fullName.substring(0, fullNameIndex);

            return fullName;
        }
        catch (IOException e)
        {
            throw new Exceptions.Warning(e.getMessage());
        }
    }

    public List<Registration> getRegistrations() throws Exceptions.UserNotLoggedIn, Exceptions.InvalidStatus, Exceptions.Warning
    {
        if (!isLogged)
            throw new Exceptions.UserNotLoggedIn();

        ArrayList<Registration> registrationsList = new ArrayList<>();
        try
        {
            // Using the registrations page to get the registrations
            //
            Document registrationsPage = session.get(domain + "/nonio/inscturmas/init.do");
            int registrationsPageStatusCode = registrationsPage.connection().response().statusCode();
            if (registrationsPageStatusCode != 200)
                throw new Exceptions.InvalidStatus(registrationsPageStatusCode);

            // Check if we are really logged in
            //
            if (!registrationsPage.location().contains("/nonio/inscturmas/init.do"))
                throw new Exceptions.UserNotLoggedIn();

            // Get the registrations
            //
            Elements registrationElements = Objects.requireNonNull(registrationsPage.select("table.displaytable").first()).select("tbody>tr");
            for (Element registrationElement: registrationElements)
            {
                Elements registrationData = registrationElement.getElementsByTag("td");
                String name = registrationData.get(0).text();
                String href = Objects.requireNonNull(registrationData.get(1).getElementsByAttribute("href").first()).attr("href");
                long id = Long.parseLong(href.substring(href.indexOf("=") + 1));

                registrationsList.add(new Registration(this, id, name));
            }
        }
        catch (IOException e)
        {
            throw new Exceptions.Warning(e.getMessage());
        }

        return registrationsList;
    }

    public List<Subject> getSubjects(long registrationId) throws Exceptions.UserNotLoggedIn, Exceptions.InvalidStatus, Exceptions.Warning
    {
        if (!isLogged)
            throw new Exceptions.UserNotLoggedIn();

        ArrayList<Subject> subjectsList = new ArrayList<>();
        try
        {
            // Using the subjects page to get the subjects
            //
            Document subjectsPage = session.get(domain + "/nonio/inscturmas/listaInscricoes.do?args=" + registrationId);
            int subjectsPageStatusCode = subjectsPage.connection().response().statusCode();
            if (subjectsPageStatusCode != 200)
                throw new Exceptions.InvalidStatus(subjectsPageStatusCode);

            // Check if we are really logged in
            //
            if (!subjectsPage.location().contains("/nonio/inscturmas/listaInscricoes.do"))
                throw new Exceptions.UserNotLoggedIn();

            // Get the subjects
            //
            Elements subjectElements = Objects.requireNonNull(subjectsPage.select("table.displaytable").first()).select("tbody>tr");
            for (Element subjectElement: subjectElements)
            {
                Elements subjectData = subjectElement.getElementsByTag("td");

                String name = subjectData.get(1).text();

                long id = 0;
                Element hrefElement = subjectData.get(6).getElementsByAttribute("href").first();
                if (hrefElement != null)
                {
                    String href = hrefElement.attr("href");
                    id = Long.parseLong(href.substring(href.indexOf("=") + 1));
                }

                long code = Long.parseLong(subjectData.get(0).text());
                int semester = Integer.parseInt(subjectData.get(2).text().substring(0, 1));

                ZonedDateTime startDate = null;
                String startDateString = subjectData.get(4).text();
                if (!startDateString.isEmpty())
                    startDate = LocalDate.parse(startDateString, dateTimeFormatter).atStartOfDay(ZoneId.systemDefault());

                ZonedDateTime endDate = null;
                String endDateString = subjectData.get(5).text();
                if (!endDateString.isEmpty())
                    endDate = LocalDate.parse(endDateString, dateTimeFormatter).atStartOfDay(ZoneId.systemDefault());

                subjectsList.add(new Subject(this, id, code, name, semester, startDate, endDate, hrefElement != null));

            }
        }
        catch (IOException e)
        {
            throw new Exceptions.Warning(e.getMessage());
        }

        return subjectsList;
    }

    public List<Class> getClasses(long subjectId) throws Exceptions.UserNotLoggedIn, Exceptions.InvalidStatus, Exceptions.InvalidClassType, Exceptions.Warning
    {
        if (!isLogged)
            throw new Exceptions.UserNotLoggedIn();

        ArrayList<Class> classList = new ArrayList<>();
        try
        {
            // Using the classes page to get the classes
            //
            Document classPage = session.get(domain + "/nonio/inscturmas/inscrever.do?args=" + subjectId);
            int classPageStatusCode = classPage.connection().response().statusCode();
            if (classPageStatusCode != 200)
                throw new Exceptions.InvalidStatus(classPageStatusCode);

            // Check if we are really logged in
            //
            if (!classPage.location().contains("/nonio/inscturmas/inscrever.do"))
                throw new Exceptions.UserNotLoggedIn();

            // Get the classes
            //
            Elements classElements = classPage.select("table.displaytable>tbody>tr");
            for (Element classElement : classElements)
            {
                Elements classData = classElement.getElementsByTag("td");
                Element scheduleInputElem = classData.get(4).getElementsByTag("input").first();
                if (scheduleInputElem == null)
                    throw new Exceptions.Warning("Error parsing class information, please report this error to the developers");

                long id = Long.parseLong(scheduleInputElem.attr("value"));

                String typeString = scheduleInputElem.attr("alt");
                Class.Type type = switch (typeString)
                {
                    case "T" -> Class.Type.THEORETICAL;
                    case "P" -> Class.Type.PRACTICAL;
                    case "TP" -> Class.Type.THEORETIC_PRACTICAL;
                    default -> throw new Exceptions.InvalidClassType(typeString);
                };

                classData.get(0).getElementsByTag("sup").remove();

                String name = classData.get(0).text();
                int num;
                if (name.contains(" "))
                    num = Integer.parseInt(name.substring(name.indexOf(typeString) + 1, name.indexOf(" ")));
                else
                    num = Integer.parseInt(name.substring(name.indexOf(typeString) + 1));

                int vacancies = Integer.parseInt(classData.get(3).text());
                int numLessons = Integer.parseInt(classData.get(2).text());

                Element inputElement = classData.get(5).getElementsByTag("input").first();

                boolean isEnrolled = false, canEnroll = false;
                if (inputElement != null)
                {
                    if (inputElement.attr("checked").equals("checked"))
                        isEnrolled = true;
                    else
                        canEnroll = true;
                }

                String teacherName = classData.get(1).text();

                classList.add(new Class(id, num, vacancies, numLessons, type, isEnrolled, canEnroll, teacherName));
            }
        }
        catch (IOException e)
        {
            throw new Exceptions.Warning(e.getMessage());
        }

        return classList;
    }

    public void enroll(long subjectId, long theoreticalId, long practicalId, long theoreticPracticalId) throws Exceptions.InvalidStatus, Exceptions.UserNotLoggedIn, Exceptions.Warning
    {
        if (!isLogged)
            throw new Exceptions.UserNotLoggedIn();

        try
        {
            // Get request subject page to get the timestamps
            //
            Document subjectPage = session.get(domain + "/nonio/inscturmas/inscrever.do?args=" + subjectId);
            int subjectPageStatusCode = subjectPage.connection().response().statusCode();
            if (subjectPageStatusCode != 200)
                throw new Exceptions.InvalidStatus(subjectPageStatusCode);

            // Check if we are really logged in
            //
            if (!subjectPage.location().contains("/nonio/inscturmas/inscrever.do"))
                throw new Exceptions.UserNotLoggedIn();

            // Set the referer header
            //
            session.setHeaders(Map.of(
                    "Content-Type", "application/x-www-form-urlencoded",
                    "Referer", domain + "/nonio/inscturmas/inscrever.do?args=" + subjectId));

            // Prepare the data
            //
            List<String> data = new ArrayList<>();
            if (theoreticalId != 0)
                data.add(String.valueOf(theoreticalId));
            if (practicalId != 0)
                data.add(String.valueOf(practicalId));
            if (theoreticPracticalId != 0)
                data.add(String.valueOf(theoreticPracticalId));

            // Send the enroll request
            //
            Document enrollResponse = session.post(domain + "/nonio/inscturmas/inscrever.do?method=submeter", "inscrever", data);

            // Clear user headers
            //
            session.clearHeaders();

            // Check if the request was successful
            //
            int enrollResponseStatusCode = enrollResponse.connection().response().statusCode();
            if (enrollResponseStatusCode != 200)
                throw new Exceptions.InvalidStatus(enrollResponseStatusCode);

            // Check if we are really logged in
            //
            if (!enrollResponse.location().contains("/nonio/inscturmas/listaInscricoes.do"))
                throw new Exceptions.UserNotLoggedIn();
        }
        catch (IOException e)
        {
            throw new Exceptions.Warning(e.getMessage());
        }
    }

    public Map<Long, Schedule> getSchedule(long subjectId) throws Exceptions.UserNotLoggedIn, Exceptions.InvalidStatus, Exceptions.Warning
    {
        if (!isLogged)
            throw new Exceptions.UserNotLoggedIn();

        Map<Long, Schedule> scheduleMap = new HashMap<>();
        try
        {
            // Get request subject page to get the timestamps
            //
            Document subjectPage = session.get(domain + "/nonio/inscturmas/inscrever.do?args=" + subjectId);
            int subjectPageStatusCode = subjectPage.connection().response().statusCode();
            if (subjectPageStatusCode != 200)
                throw new Exceptions.InvalidStatus(subjectPageStatusCode);

            // Check if we are really logged in
            //
            if (!subjectPage.location().contains("/nonio/inscturmas/inscrever.do"))
                throw new Exceptions.UserNotLoggedIn();

            // Set the content type header
            //
            session.setHeaders(Map.of(
                    "Accept" , "application/json",
                    "Referer", domain + "/nonio/inscturmas/inscrever.do?args=" + subjectId,
                    "Content-Type", "application/x-www-form-urlencoded"));

            // Get timestamps
            //
            long now = CalendarTimestamps.getCurrentTimestamp();
            long startDay = CalendarTimestamps.getTimestampFromLocalDate(getCurrentDateTime().with(TemporalAdjusters.firstDayOfMonth()).toLocalDate());
            long endDay = CalendarTimestamps.getTimestampFromLocalDate(getCurrentDateTime().with(TemporalAdjusters.lastDayOfMonth()).toLocalDate());

            // Requesting the schedule page to get the schedule
            //
            Document schedulePage = session.get(domain + "/nonio/inscturmas/inscrever.do?method=actualizaDados&notReset=true&_=" + now + "&start=" + startDay + "&end=" + endDay);

            // Clear the Accept header
            //
            session.clearHeaders();

            int schedulePageStatusCode = schedulePage.connection().response().statusCode();
            if (schedulePageStatusCode != 200)
                throw new Exceptions.InvalidStatus(schedulePageStatusCode);

            // Check if we are really logged in
            //
            if (!schedulePage.location().contains("/nonio/inscturmas/inscrever.do"))
                throw new Exceptions.UserNotLoggedIn();

            // Read each object from the JSON
            //
            JSONArray scheduleJson = new JSONArray(schedulePage.body().text());
            for (Object scheduleObject : scheduleJson)
            {
                JSONObject scheduleJsonObject = (JSONObject) scheduleObject;

                Long id = scheduleJsonObject.getLong("id");
                ZonedDateTime start = CalendarTimestamps.getZonedDateTimeFromStr(scheduleJsonObject.getString("start"));
                ZonedDateTime end = CalendarTimestamps.getZonedDateTimeFromStr(scheduleJsonObject.getString("end"));

                scheduleMap.put(id, new Schedule(start.getDayOfWeek(), start.toLocalTime(), end.toLocalTime()));
            }
        }
        catch (IOException e)
        {
            throw new Exceptions.Warning(e.getMessage());
        }

        return scheduleMap;
    }
}