package pt.isec.gps_g22_ace.ace;

import org.jsoup.Connection;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;

import java.io.IOException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class Session
{
    private final int timeout;
    private final String userAgent;

    private Map<String, String> userHeaders;
    private Map<String, String> userCookies, responseCookies;

    public Session()
    {
        responseCookies = new HashMap<>();

        this.timeout = 10000;
        this.userAgent = "Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/51.0.2704.103 Safari/537.36";
    }

    private Document processResponse(Document doc)
    {
        Map<String, String> newResponseCookies = doc.connection().response().cookies();

        if (responseCookies == null)
            responseCookies = new HashMap<>();

        responseCookies.putAll(newResponseCookies);

        return doc;
    }

    Document get(String url) throws IOException
    {
        return get(url, true);
    }
    Document get(String url, boolean followRedirects) throws IOException
    {
        return processResponse(Jsoup.connect(url).ignoreContentType(true).timeout(timeout).userAgent(userAgent).method(Connection.Method.GET).headers(getHeaders()).followRedirects(followRedirects).cookies(getCookies()).get());
    }

    Document post(String url, Map<String, String> data) throws IOException
    {
        return processResponse(Jsoup.connect(url).timeout(timeout).userAgent(userAgent).method(Connection.Method.POST).headers(getHeaders()).cookies(getCookies()).data(data).post());
    }

    Document post(String url, String keyName, List<String> data) throws IOException
    {
        Connection connection = Jsoup.connect(url).timeout(timeout).userAgent(userAgent).method(Connection.Method.POST).headers(getHeaders()).cookies(getCookies());

        for (String value : data)
            connection.data(keyName, value);

        return processResponse(connection.post());
    }

    void setHeaders(Map<String, String> headers)
    {
        this.userHeaders = headers;
    }

    void setCookies(Map<String, String> cookies)
    {
        this.userCookies = cookies;
    }

    Map<String, String> getHeaders()
    {
        Map<String, String> ret = new HashMap<>();

        if (userHeaders != null)
            ret.putAll(userHeaders);

        return ret;
    }

    Map<String, String> getCookies()
    {
        Map<String, String> ret = new HashMap<>();

        if (userCookies != null)
            ret.putAll(userCookies);
        if (responseCookies != null)
            ret.putAll(responseCookies);

        return ret;
    }

    void clearHeaders()
    {
        if (userHeaders != null)
            userHeaders = null;
    }

    void clearCookies()
    {
        if (userCookies != null)
            userCookies = null;
    }

    void clearSessionCookies()
    {
        if (responseCookies != null)
            responseCookies = null;
    }

    void clearAllCookies()
    {
        clearCookies();
        clearSessionCookies();
    }

    void clearAll()
    {
        clearHeaders();
        clearAllCookies();
    }
}
