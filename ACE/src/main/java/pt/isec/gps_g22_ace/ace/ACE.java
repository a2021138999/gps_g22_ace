package pt.isec.gps_g22_ace.ace;

import pt.isec.gps_g22_ace.ace.data.*;
import pt.isec.gps_g22_ace.ace.data.Class;

import java.awt.image.BufferedImage;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;
import java.util.Map;

public class ACE
{
    private static ACE _instance;

    private ACE()
    {
    }

    public static ACE getInstance()
    {
        if (_instance == null)
            _instance = new ACE();

        return _instance;
    }

    private NonioScraper scraper = null;

    private String lastError;

    public String getLastError()
    {
        return lastError;
    }

    private void setLastError(String error)
    {
        lastError = error;
    }

    private String domain = "http://127.0.0.1:5000";

    public String getDomain()
    {
        return domain;
    }

    public boolean setDomain(String domain)
    {
        if (scraper != null)
            return false;

        // Check if domain is valid
        //
        if (!isValidDomain(domain))
            return false;

        // Remove trailing slash
        //
        if (domain.endsWith("/"))
            domain = domain.substring(0, domain.length() - 1);

        this.domain = domain;
        return true;
    }

    private boolean isValidDomain(String url)
    {
        try
        {
            URI uri = new URI(url);
            String path = uri.getPath();
            String query = uri.getQuery();
            String fragment = uri.getFragment();

            // Check if path, query, and fragment are empty
            return path == null || path.isEmpty() &&
                    query == null || query.isEmpty() &&
                    fragment == null || fragment.isEmpty();
        }
        catch (URISyntaxException e)
        {
            return false;
        }
    }

    public boolean login(String email, String password)
    {
        return login(email, password, null);
    }

    public boolean login(String email, String password, String captchaAnswer)
    {
        if (scraper == null)
            scraper = new NonioScraper(domain);
        else
            System.out.println("Scraper already initialized");  // This should never happen

        try
        {
            scraper.login(email, password, captchaAnswer);
        }
        catch (Exceptions.UserAlreadyLoggedIn e)
        {
            System.out.println("User already logged in");
        }
        catch (Exceptions.Warning | Exceptions.InvalidStatus e)
        {
            scraper = null;
            setLastError(e.getMessage());
            return false;
        }

        user = new User(scraper);
        return true;
    }

    public boolean logout()
    {
        try
        {
            scraper.logout();
        }
        catch (Exceptions.UserNotLoggedIn e)
        {
            System.out.println("User not logged in");
            return true;
        }
        catch (Exceptions.InvalidStatus e)
        {
            System.out.println("Invalid status: " + e.getMessage());
            setLastError(e.getMessage());
            return false;
        }
        catch (NullPointerException e)  // This should never happen
        {
            System.out.println("Scraper not initialized");
            return true;
        }
        catch (Exceptions.Warning e)
        {
            System.out.println("Warning: " + e.getMessage());
            setLastError(e.getMessage());
            return false;
        }
        finally
        {
            user = null;
            scraper = null;
        }

        return true;
    }

    public BufferedImage getCaptchaImage()
    {
        if (scraper == null)
            return null;

        try
        {
            return scraper.getCaptchaImage();
        }
        catch (Exceptions.Warning e)
        {
            System.out.println("Warning: " + e.getMessage());
            setLastError(e.getMessage());
            return null;
        }
    }

    public boolean needsCaptcha()
    {
        if (scraper == null)
            return false;

        return scraper.needsCaptcha();
    }

    public boolean isLogged()
    {
        if (scraper == null)
            return false;

        return scraper.isLogged();
    }

    private User user = null;

    public User getUser()
    {
        return user;
    }

    public Map<Subject, List<Class>> applyAlgorithm(Preferences preferences)
    {
        try
        {
            return new SearchAlgorithm().search(preferences);
        }
        catch (Exceptions.NoClassesAvailable e)
        {
            System.out.println("No classes available");
            setLastError(e.getMessage());
            return null;
        }
    }

}
