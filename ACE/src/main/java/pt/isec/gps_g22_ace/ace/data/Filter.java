package pt.isec.gps_g22_ace.ace.data;

import java.io.Serial;
import java.io.Serializable;

public class Filter implements Serializable
{
    public enum Type
    {
        EXCLUDE_SCHEDULE,
        INCLUDE_SCHEDULE
    }

    @Serial
    private static final long serialVersionUID = 2L;

    private final Type type;
    private final Object data;

    public Filter(Type type)
    {
        this.data = null;
        this.type = type;
    }

    public Filter(Type type, Object data)
    {
        this.type = type;
        this.data = data;
    }

    public boolean apply(Class c)
    {
        return switch (type)
        {
            case EXCLUDE_SCHEDULE -> !isInSchedule(this, c);
            case INCLUDE_SCHEDULE -> isInSchedule(this, c);
        };
    }

    private static boolean isInSchedule(Filter _this, Class c)
    {
        Schedule classSchedule = c.getSchedule();
        Schedule filterSchedule = (Schedule) _this.data;
        return classSchedule.overlaps(filterSchedule);
    }
}
