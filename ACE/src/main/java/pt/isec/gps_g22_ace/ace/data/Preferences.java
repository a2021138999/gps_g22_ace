package pt.isec.gps_g22_ace.ace.data;

import pt.isec.gps_g22_ace.utils.RecordingManager;

import java.io.Serial;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

public class Preferences implements Serializable
{
    @Serial
    private static final long serialVersionUID = 1L;
    public Preferences()
    {

    }

    private final List<Filter> filterList = new ArrayList<>();

    public List<Filter> getFilterList()
    {
        return filterList;
    }

    public void addFilter(Filter filter)
    {
        filterList.add(filter);
    }

    public void removeFilter(Filter filter)
    {
        filterList.remove(filter);
    }

    public boolean saveMeInto(String fileName)
    {
        return RecordingManager.getInstance().writeToFile(fileName,this);
    }

    public void loadMeFrom(String fileName)
    {
        Preferences p = (Preferences) RecordingManager.getInstance().loadFromFile(fileName);
        //TODO: Colocar os valores vindos de p neste objeto (this)
    }
}
