package pt.isec.gps_g22_ace.ace.data;

public class Class
{
    public enum Type
    {
        THEORETICAL,
        PRACTICAL,
        THEORETIC_PRACTICAL;

        public String getName()
        {
            return switch (this)
            {
                case THEORETICAL -> "T";
                case PRACTICAL -> "P";
                case THEORETIC_PRACTICAL -> "TP";
            };
        }

        public static Type get(String name)
        {
            return switch (name)
            {
                case "T" -> THEORETICAL;
                case "P" -> PRACTICAL;
                case "TP" -> THEORETIC_PRACTICAL;
                default -> null;
            };
        }
    }

    private final long id;
    private final int number, vacancies, numLessons;
    private final Type type;
    private boolean isEnrolled;
    private final boolean canEnroll;
    private final String teacherName;
    private Schedule schedule = null;

    public Class(long id, int number, int vacancies, int numLessons, Type type, boolean isEnrolled, boolean canEnroll, String teacherName)
    {
        this.id = id;
        this.number = number;
        this.vacancies = vacancies;
        this.numLessons = numLessons;
        this.type = type;
        this.isEnrolled = isEnrolled;
        this.canEnroll = canEnroll;
        this.teacherName = teacherName;
    }

    public String getName()
    {
        return type.getName() + number;
    }

    public long getId()
    {
        return id;
    }

    public int getNumber()
    {
        return number;
    }

    public int getVacancies()
    {
        return vacancies;
    }

    public int getNumLessons()
    {
        return numLessons;
    }

    public Type getType()
    {
        return type;
    }

    public boolean isEnrolled()
    {
        return isEnrolled;
    }

    public void setEnrolled(boolean isEnrolled)
    {
        this.isEnrolled = isEnrolled;
    }

    public boolean canEnroll()
    {
        return canEnroll && vacancies > 0;
    }

    public String getTeacherName()
    {
        return teacherName;
    }

    public void setSchedule(Schedule schedule)
    {
        this.schedule = schedule;
    }

    public Schedule getSchedule()
    {
        return schedule;
    }

    @Override
    public boolean equals(Object obj)
    {
        if (obj == null)
            return false;

        if (!(obj instanceof Class c))
            return false;

        return c.getId() == id;
    }
}
