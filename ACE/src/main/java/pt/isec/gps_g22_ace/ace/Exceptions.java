package pt.isec.gps_g22_ace.ace;

public class Exceptions
{
    public static class UserAlreadyLoggedIn extends Exception
    {
        public UserAlreadyLoggedIn()
        {
            super("User already logged in");
        }
    }

    public static class UserNotLoggedIn extends Exception
    {
        public UserNotLoggedIn()
        {
            super("User not logged in");
        }
    }

    public static class Warning extends Exception
    {
        public Warning(String message)
        {
            super(message);
        }
    }

    public static class InvalidStatus extends Exception
    {
        public InvalidStatus(int status)
        {
            super("Invalid status: " + status);
        }
    }

    public static class InvalidClassType extends Exception
    {
        public InvalidClassType(String type)
        {
            super("Invalid class type: " + type);
        }
    }

    public static class NoClassesAvailable extends Exception
    {
        public NoClassesAvailable()
        {
            super("No classes available");
        }
    }
}
