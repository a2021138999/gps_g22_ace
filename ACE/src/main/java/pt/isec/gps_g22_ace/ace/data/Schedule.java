package pt.isec.gps_g22_ace.ace.data;

import java.time.DayOfWeek;
import java.time.LocalTime;

public class Schedule
{
    private final DayOfWeek day;
    private final LocalTime start, end;

    public Schedule(DayOfWeek day, LocalTime start, int durationMinutes)
    {
        this.day = day;
        this.start = start;
        this.end = start.plusMinutes(durationMinutes);
    }

    public Schedule(DayOfWeek day, LocalTime start, LocalTime end)
    {
        this.day = day;
        this.end = end;
        this.start = start;
    }

    public LocalTime getStartTime()
    {
        return start;
    }

    public LocalTime getEndTime()
    {
        return end;
    }

    public DayOfWeek getDayOfWeek()
    {
        return day;
    }

    public int getDuration()
    {
        return (int) java.time.Duration.between(start, end).toMinutes();
    }

    public boolean overlaps(Schedule other)
    {
        return day == other.day &&
                (start.isBefore(other.end) && end.isAfter(other.start)) ||
                (other.start.isBefore(end) && other.end.isAfter(start));
    }


    @Override
    public String toString()
    {
        return "Schedule{" +
                "start=" + start +
                ", end=" + end +
                '}';
    }

    @Override
    public boolean equals(Object obj)
    {
        if (obj == null)
            return false;

        if (obj == this)
            return true;

        if (!(obj instanceof Schedule other))
            return false;

        return day == other.day &&
                start.equals(other.start) &&
                end.equals(other.end);
    }
}
